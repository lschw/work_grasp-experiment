Work: Grasp Experiment
===============================
Contains code used to control the UR5 and Schunk PG70 through the CAROS interface.
The controls is inbuild in a RobWorkStudio plugin for control.
Also features the possibility to interface to a camera, but requires OpenCV.

To see how it works see the [usermanual](manpages.md).


Requirements:
-------
 - RobWorkStudio
 - RobworkHardware
 - CAROS
 - OpenCV (Optional)
 
 
 

 
