# Usermanual #

Contents:

 - [Installation](#installation)
 - [The Plugin](#the-plugin)
   - [Settings](#settings)
   - [Robot](#robot)
   - [Gripper](#gripper)
   - [Camera](#camera)
   - [Experiment](#experiment)
   - [Forcemode](#forcemode)
   - [Remote Control](#remote-control)





# Installation #

First install the dependencies: [RobWorkStudio](http://www.robwork.dk/apidoc/nightly/rw/), [RobWorkHardware](http://www.robwork.dk/apidoc/nightly/rw/) and [CAROS](https://gitlab.com/caro-sdu/caros/wikis/home).
Optional dependecies are: [OpenCV](http://opencv.org/) for use with camera.

Furthermore the patches located in work_grasp-experiment/patches/ have to be applied to RobWorkHardware and CAROS.

To install:

```sh
cd .../work_grasp-experiment/robotControl/
mkdir build
cd build/
cmake ..
make -j2

```

After having run the "cmake" command, the user will be able to see if the computer vision part of the plugin was enabled or disabled from the command line output:
 "-- Vision system enabled."
   or
 "-- Vision system disabled.".




# The Plugin #

The plugin is made to conduct a set of grasping experiments, with or without vision ontop of it to quantify the grasping quality.
In order to use the plugin, first the user connects to the hardware to be used using the settings tab.
Doing so, also enables some of the other tabs, depending on the hardware connected to.
The computer vision part of the program (connecting to camera and using the *Camera* tab) is only enabled if OpenCV was found during the installation.


## Settings ##

The *Settings* tab is used to connect to the different types of hardware.

This is done Using the following steps:
 - Enter the name of the device/frame of the object in the workcell loaded into *Device Name*.
 - Enter the namespace to the rosnode into *Namespace*.
 - Press the *(Re)Connect ...* button to connect to the hardware.
 
If the connection was successfully established this will result in the enabling of the tab concerning that hardware type and the status bar will also indicate such.
In the case of the program losing connection to the hardware, then the concerned tabs will be disabled.
In general if any rosnode dies to which the plugin is connected, restart it and then reconect.
However, there are cases where the loss of control of the hardware can not be detected from the program.

This is the case for the robot if it is a UR and it is jogged using the teach panel (this disables the script running on the UR).
In such cases restart the UR rosnode and reconnect to the robot.

If the camera dies, this may not be noted before the plugin also requests a new image.
In such cases reconnect to the hardware again.

Some times when connection has been lost to some hardware, the controls may not repspond to your input.
This can sometimes be solved by invoking the *STOP Robot* button and enabling it again (Doing so empties the task que).

When connecting to hardware the default settings can also be used to quickly (re)connect to the hardware.
Once a 'unknown' piece of hardware has been connected to, it will be listed under *Default Settings* as well, but only for the current session.



## Robot ##

Used for general control of the robot.
All the speeds send to the robot can be reduced using the *Robot Speed Modifier*.

The most recent path send to the robot will be placed in the rw playback.

In case of the *Trust Robot To Move Independently* is not ticked, then the user is required to press *Accept Showed Path* for each movement the robot is to make.
The trajectory it will follow is shown in the rw playback tool.
If the path is not to be executed, the user should press *STOP Robot*.


## Gripper ##

This tab is used to control the gripper.

**Close**:
Sends the close command to the gripper with the provided force value.

**Open**: 
Sends the open command with the given distance value.

**Home**:
Calls the open command with a value of 0.034 meters.


## Camera ##

The *Camera* tab enables a wide range of input to the algorithm run with the camera.
This plugin uses the implementation of [OpenCV's Template Matching Algorithm](http://docs.opencv.org/3.2.0/de/da9/tutorial_template_matching.html) for the vision inspection.

The frame of the camera is defined so the z-axis points out of the camera in direction of the field of view.
The x-axis points to the right and the y-axis down in the image frame.
Note that these are the same conventions used by [ROS Camera Info](http://wiki.ros.org/image_pipeline/CameraInfo).

### Camera Algorithm Settings

**Load Templates**: 
Loads the templates located in the folder specified.
These should contain the following: a set of template images (.png), a set of cropping masks (.png) and a set of files with the transformation of the object used to aquire the template image.
To get this from a CAD file, the [template creater in COVIS](https://gitlab.com/caro-sdu/covis-app/tree/master/create_templates) can be used to genereate these.
The format used for the files are those corresponding to the output of the COVIS template creator.

**Image Type**: 
What the image from the camera and the templates should be converted to, before matching them.
At the time of this writing 'RAW Image' (no change to the image) and the edge detectors 'Canny Edges', 'Sobel Edges' and 'Laplacian Edges' are available.

**Obj. BB Halflength**: 
Specifies the expected volume in which the object is expected to be found.
The center of the BB is taken as the tcp of the robot and the image is then cropped as to only consider this BB.
The BB is axis aligned with the camera frame.

**Run Vision Algorithm**: 
Captures a image and runs the vision algorithm on it. 
In order to do so it is needed to have found the tcp frame ín the workcell.
This is the case because the tcp frame is used to calculate the BB used for cropping the image based on the robots position.
To locate the tcp frame, chose the tab *Experiment* and edit the entry in *TCP*, then press *Set Origo*.

**Image Storage Folder**: 
Sets the location (folder) in which to store the images captured from the camera that have been used to locate the object.
It stores both the raw image and an image indicating where the object was found in the image.
Not setting this variable will simply cause the images not to be saved.

**MM**: 
Matching method, is the algorithm used to find the best matching template. 
[See OpenCV's guide](http://docs.opencv.org/3.2.0/de/da9/tutorial_template_matching.html) for explaination of the methods.

 - SQDIFF: 
```math
 R(x,y) = \Sigma_{x',y'} (T(x',y') - I(x + x', y + y'))^2
```

 - CCORR:
```math
 R(x,y) = \Sigma_{x',y'} (T(x',y') \cdot I(x + x', y + y'))
```

 - CCOEFF
```math
 R(x,y) = \Sigma_{x',y'} (T'(x',y') \cdot I'(x+x', y+y'))
```
where 
```math
 T'(x', y') = T(x', y') - 1 / (w \cdot h) \cdot \Sigma_{x'', y''} T(x'', y'') 
```
```math 
 I'(x + x', y + y') = I(x + x', y + y') - 1 / (w \cdot h) \cdot \Sigma_{x'', y''} I(x + x'', y + y'')
```

**Record Static Bg. Img.**:
Records the current image from the camera and uses it to subtract from all future images before locating the object on such.
This is usefull for cluttered backgrounds where a lot of static objects are present.
The image should be updated when light conditions and/or the image settings change.
Preferably the robot and object is moved out of the field of view of the camera before recording the image.

**Smooth Template Matches**:
When checked the resulting image with the scores of the template matching for one template on the image is smoothed before the min/max is located.
The smoothening is done with a hardcoded sigma (at the time of this writing $` \sigma = 1 `$ ).
Applying smoothening to the image stabilises the result of the template matcher under the assumption of locality when fitting a template, whereas it does not apply for noise.


### Camera Settings

**Show Detection Output**:
When ticked it shows the output of the vision algorithm in a set of windows.

**Show Live Camera View**:
Opens a window and continously updates it with a new image from the camera as long as it is enabled.
This runs at a slow refresh rate.

**Connect to Info**:
Subscribes to the ROS camera info topic specified.
The information of the camera intrinsics is then used for the calculations of the BB used for cropping.


### Camera to Object Transform
Sets the transform to which the robot moves in between the experiments to verify the objects position in the gripper.
The robot moves its own TCP frame to the location specified by the transform $` T_{cam}^{obj} `$ so that:

```math
T_{rbase}^{TCP} = T_{rbase}^{cam} \cdot T_{cam}^{obj}
```

where *rbase* is the robot base, *cam* the camera, *obj* the object and *TCP* the TCP frame of the robot.

The rotation is specified in degrees and trasnlation in meters.


## Experiment ##

The *Experiment* tab is used for the conducting of the experiments.
To do so, it follows a cycle of instructions it runs through.

These are:
  1. Go to origo.
  2. (G) Open gripper.
  3. Retract from the origo pose.
  4. Move to the approach position.
  5. Approach to the specified pickup pose.
  6. (G) Close the gripper.
  7. Lift the object.
  8. (C) Move infront of camera.
  9. (C) Detect the object.
  10. Classify the grasp.

where (C) and (G) indicate optional steps that are only enabled when the camera and gripper are connected respectively.

Below are the explainations of the UI concerning the *Experiment* tab in general.


**Load Task File**:
Loads a .csv file containing the data used for the experiments.
The data contains information about the relative grasp transform compared to origo and the expected grasp result.
This is given by a n * 7 matrix in the csv file where the first six entries in each row is the transform and the last the grasp result.
Each row in the file thus contain the data in the following format [x, y, z, roll, pitch, yaw, result] in that order.
The translational parts (x, y, z) are given in meters and the rotational (roll, pitch, yaw) in degrees.
The right hand coordinate system is used.

The result can be: 0 = failure, 1 = misalignment, 2 = success and 3 = maybe.
Other numbers may be entered, but this is the defined results.

*Task Repeats* can be used to conduct each test in the task file multiple times.


**Set Origo**:
Sets the origo of the robot (the base grasp pose) to be the current location of the physical robot.
Finds the frames indicated in the *TCP* and *Base* entry boxes.

**Start Test**:
Enables and starts the test by executing step 1 to 9 for the first test entry.

**Stop Test**:
Stops the test and stores the result the same place as the task file and with the same name, except for an appended '_result.csv'.
In case the vision system is enabled the vision results are stored likewise, but with '_result_vision.csv' appended to the name.


**Gripper Approach Q**:
Defines the Q value the gripper should be set at when approaching the object in step 5.

**Retract distance**:
The distance the gripper retracts from origo and the next grasp pose in step 3 and 4 respectively.
Retraction is done along the z-axis of the gripper in the negative direction.

**Lift Height**:
The distance to lift the object in step 7.

**Restart Test**:
Reruns the test (step 1 to 9) for the given test index (the number in the spin box next to the status bar).

**Release Obj.**:
Opens the gripper to the *Gripper Approach Q* value.

**Return to Origo**:
Moves the robot to origo and opens the gripper to the *Gripper Approach Q* value.


**Failure**, **Misalignment**, **Maybe** & **Success**:
Used in step 10 to classify the result of the test.
Sets the result according to the button pressed (failure = 0, misalignment = 1, success = 2 and maybe = 3) and advances the test count.

If the grasp was marked successfully, the robot is moved back to origo.
In all cases the gripper is opened to the *Gripper Approach Q* value and the robot is moved back to origo.


**The status bar**:
Indicates how many test are conducted and gives the option to jump back and forth in the tests using the spin box to its right.


**Lift Straight Up**:
If ticked, the robot moves the gripper straight up in the positive direction of the z-axis of the world frame.
Otherwise the robot moves in the positive direction of the z-axis of the tcp frame.


**moveLin Specified with Q**:
If checked the move commands to the robot making it move linearly in Cartesian space are send using the Q interface instead of the Transform3D.

This option is available because the accuracy of the robots final position using the move command may vary depending on how old the robot is.
(A larger offset was experienced in the robots final position when using the interface taking in 3D transforms.)


**use moveLin for retract**:
If checked the robot will also retract moving linearly in Cartesian space instead of configuration space.



 
## Forcemode ##
Used to play with the forcemode of the UR.
If *Forcemode Enabled* is checked, then the robot will activate the forcemode during the grasp of the experiment (step 6).

The forcemode can be toggled on and off, when enabled, using the *Toggle Forcemode*.

See the UR manual for details.


## Remote Control ##

Is used to grasp an object at a transform streamed on a ROS topic with the data type *geometry_msgs::Transform*.
To do so, enter the topic name in *T. Topic Name* and check *Recieve T.* to recieve the transforms.
*Transform Post Modification* can be used to apply an offset to the transform recieved (specified in degrees and meters).

Pressing *Accept Grasp Transform* will then make the robot grasp the object at the recieved pose and move it to a hardcoded place in the workspace.
This location is currently hardcoded in the code of the plugin (this should ideally be made to a frame in the workcell instead).

**Gripper Q Appr.**:
Specifies the gripper configuration to use when grasping.

**Lin. Appr.**:
The distance to approach the object in linearly in cartesian space from the z-direction.

**Ref. Frame**:
The frame in the workcell used as the reference to the transforms from the topic.

**Object Name**:
The frame in the workcell used as the one updated by the transform recieved and grasped.

**Pre-Appr. Obj.**:
The frame used as collidable geometry before the approach and grasp phase is initiated.

 
 
 