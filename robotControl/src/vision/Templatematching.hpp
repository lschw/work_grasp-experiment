#pragma once

#include <rw/math/Transform3D.hpp>

#include <opencv2/core/core.hpp>


#include <vector>
#include <string>


namespace vision {

class Templatematching
{
   
 public:
     
     struct CannyOptions {
         double _lowerTreshold = 10.0, _upperTreshold = 30;
         int _apertureSize = 3;
         bool _l2gradient = false;
     };

     struct LaplaceOptions {
         int _apertureSize = 3;
     };

     struct SobelOptions {
         int _apertureSize = 1, _xorder = 1, _yorder = 1;
     };
     
     enum ImageMatchingMethod {
         RAW_IMAGE, CANNY_EDGES, LAPLACIAN_EDGES, SOBEL_EDGES
     };
     
     
     struct Template {
       rw::math::Transform3D<> transform;
       cv::Mat templateImg;
       std::string origImg = "";
       cv::Rect mask;
       cv::Point cog;
       bool greyImage = false;
     };
     
     
     Templatematching();
     
     virtual ~Templatematching(){}
     
     /**
      * @brief Loads the template data given.
      * 
      * @param images The image templates.
      * @param imgmask The mask for the image (smallest AABB, mirrored along the x-axis).
      * @param transform Transformations by which the obj was trasnformed.
      * @return void
      */
     void loadImages(const std::vector< std::string > &images, const std::vector< std::string > &imgmask, const std::vector< std::string > &transform, const bool greyImg = true);
     
     
     std::vector< Template > getTemplates() const {
         return _templates;
     }

     const std::vector< Template >& getTemplates() {
         return _templates;
     }

     
     /**
      * @brief Finds the best matching template and returns its index.
      * 
      * @param img The image to consider (The image is converted from raw to the specified type using "convertImage" when given).
      * @return int Index of best fit.
      */
     int matchTemplate(const cv::Mat &img, cv::Rect &result, double &score) const;
     
     
     /**
      * @brief Sets the method used for Template matching:
      *         CV_TM_SQDIFF, CV_TM_SQDIFF_NORMED, CV_TM_CCORR,
      *         CV_TM_CCORR_NORMED, CV_TM_CCOEFF, CV_TM_CCOEFF_NORMED.
      * 
      * @return void
      */
     void setMatchingMethod(const int method){
         _templateMatchingMethod = method;
     }
     
     void setImageMatchingMethod(const ImageMatchingMethod method);
     
     ImageMatchingMethod getImageMatchingMethod() {
         return _imageMatchingMethod;
     }
     
     void setCannyOptions(const CannyOptions &opt);
     void setSobelOptions(const SobelOptions &opt);
     void setLaplacianOptions(const LaplaceOptions &opt);
     
     /**
      * @brief Converts the image ot the type used. This is automatically done when using "matchTemplate" and should thereofre not be used on the input image.
      * 
      * @param img ...
      * @return void
      */
     
     void convertImage(cv::Mat &img) const;
     
     void smoothTemplateMatches(const bool smooth);
     
 private:
     
     /**
      * @brief Converts the image to the appropiate format.
      * 
      * @param img 
      * @return void
      */
     
     void convertToCannyEdges(cv::Mat &img) const;

     void convertToLaplacianEdges(cv::Mat &img) const;
     
     void convertToSobelEdges(cv::Mat &img) const;
     
     rw::math::Transform3D<> loadTransform(const std::string file) const;

     cv::Rect findRectangle(const cv::Mat imgmask) const;     
     
     
     std::vector< Template > _templates;
     
     int _templateMatchingMethod;
     
     bool _smoothTemplateMatches;
     
     ImageMatchingMethod _imageMatchingMethod;
     CannyOptions _cannyParams;
     SobelOptions _sobelParams;
     LaplaceOptions _laplacianParams;
    
};

}