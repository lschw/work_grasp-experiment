#pragma once

#include <QWidget>

#include "vision/Templatematching.hpp"

#include "BasicOption.hpp"

namespace widget {
    namespace vision {
        
        
        
        class CannyImageOption : public BasicOption {
        public:
            
            CannyImageOption(QWidget &widget, ::vision::Templatematching &algo);
            
            virtual ~CannyImageOption();
            
            
            virtual void updateTemplateMatcher();
            
            
        protected:
            
            virtual void drawWidget();
                        
            void getCannyEdgeOptions(::vision::Templatematching::CannyOptions &options);
            
            
        };
    }
}

