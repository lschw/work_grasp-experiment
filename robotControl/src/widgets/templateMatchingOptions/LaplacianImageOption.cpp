#include "LaplacianImageOption.hpp"

#include <QSlider>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>


widget::vision::LaplacianImageOption::LaplacianImageOption ( QWidget& widget, ::vision::Templatematching& algo ) : BasicOption ( widget, algo ) {
    if(_algorithm.getImageMatchingMethod() != ::vision::Templatematching::RAW_IMAGE) {
        _algorithm.setImageMatchingMethod(::vision::Templatematching::RAW_IMAGE);
    }
    _algorithm.setImageMatchingMethod(::vision::Templatematching::LAPLACIAN_EDGES);
    
    drawWidget();
}

widget::vision::LaplacianImageOption::~LaplacianImageOption() {

}

void widget::vision::LaplacianImageOption::drawWidget() {
    // add layout http://doc.qt.io/qt-4.8/qhboxlayout.html
    QGridLayout *layout = new QGridLayout();
    _widgetsObjects.push_front(layout);

    const int minHeight = 25;
    // Slider for apperature
    QSlider* aperture = new QSlider(Qt::Horizontal);
    _widgetsObjects.push_front(aperture);
    aperture->setRange(1, 3);
    aperture->setObjectName("apperture");
    aperture->setValue(1);
    aperture->setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
    aperture->setMinimumHeight(minHeight);
    layout->addWidget(aperture,0,1);
    QLabel *laperture = new QLabel("Apperture");
    _widgetsObjects.push_front(laperture);
    layout->addWidget(laperture,0,0);
    
    // Btn to upload the settings
    _trigger = new QPushButton("Update Laplacian Settings");
    _widgetsObjects.push_front(_trigger);
    _trigger->setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
    _trigger->setMinimumHeight(minHeight);
    layout->addWidget(_trigger,1,0,1,-1);

    // add the layout and update the plugin
    _widget.setLayout(layout);
    _widget.setMinimumHeight(minHeight * 3);
    _widget.setSizePolicy(QSizePolicy::Policy::Expanding , QSizePolicy::Policy::Preferred );
}

void widget::vision::LaplacianImageOption::updateTemplateMatcher() {
    ::vision::Templatematching::LaplaceOptions opt;
    getLaplacianEdgeOptions(opt);
    _algorithm.setLaplacianOptions(opt);
}

void widget::vision::LaplacianImageOption::getLaplacianEdgeOptions ( ::vision::Templatematching::LaplaceOptions& options ) {
    // search for the objects added
    int i = 0;
    for(std::list< QObject* >::iterator it = _widgetsObjects.begin(); it != _widgetsObjects.end(); it++){
        if((*it)->objectName() == "apperture" && dynamic_cast< QSlider* >(*it) != NULL){
            options._apertureSize = dynamic_cast< QSlider* >(*it)->value() * 2.0 + 1.0;
            i++;
        }
    }
    if(i != 1){
        throw std::runtime_error("Not all options were found.");
    }
}



