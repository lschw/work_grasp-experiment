#pragma once

#include <QWidget>

#include "vision/Templatematching.hpp"

#include "BasicOption.hpp"

namespace widget {
    namespace vision {
        
        
        
        class LaplacianImageOption : public BasicOption {
        public:
            
            LaplacianImageOption(QWidget &widget, ::vision::Templatematching &algo);
            
            virtual ~LaplacianImageOption();
            
            
            virtual void updateTemplateMatcher();
            
        protected:
            
            void getLaplacianEdgeOptions(::vision::Templatematching::LaplaceOptions &options);
            
            virtual void drawWidget();
                        
            
            
        };
    }
}

