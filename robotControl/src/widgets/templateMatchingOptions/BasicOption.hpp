#pragma once

#include <QWidget>
#include <QObject>
#include <QPushButton>

#include "vision/Templatematching.hpp"

namespace widget {
    namespace vision {
        
        
        
        class BasicOption {
        public:
            
            BasicOption(QWidget &widget, ::vision::Templatematching &algo);
            
            virtual ~BasicOption();
            
            
            virtual void updateTemplateMatcher() = 0;
            
            virtual QPushButton* getTriggerButton();
            
        protected:
            
            virtual void drawWidget() = 0;
            
            virtual void clearOptionWidget();
            
            QWidget &_widget;
            ::vision::Templatematching &_algorithm;
            std::list< QObject* > _widgetsObjects;
            QPushButton *_trigger;
            
        };
    }
}