#include "robot.hpp"

#include <rw/common/Log.hpp>


hardware::Robot::Robot(const std::string actingNamespace) : 
_nodehandle("~"), _sdsip(_nodehandle, actingNamespace)
  // sdsip needs the name of the node to connect to, in this case the caros_ur..
{
    _connected = false;
    // connect to robot unless it takes too long
    _configuration = getCurrentJointConfiguration();
    
    // if connected, set true
    if(!_connected){
        rw::common::Log::log().info() << "Could not connect to robot!" << std::endl;
    }
}

hardware::Robot::~Robot(){}

bool hardware::Robot::isOK() const {
    return _connected;
}

bool hardware::Robot::movePtp(const rw::math::Q &pos, const double &speed, const double &blend) {
    if(!_connected){
        rw::common::Log::log().info() << "ERROR: Not connected!" << std::endl;
        return false;
    }
    rw::trajectory::QPath path;
    path.emplace_back(pos);
    std::vector<double> s = {speed}, b = {blend};
    return doFollowPath(path, s, b);
}

bool hardware::Robot::movePtp(const rw::trajectory::QPath &path) {
    if(!_connected){
        rw::common::Log::log().info() << "ERROR: Not connected!" << std::endl;
        return false;
    } 
    return doFollowPath(path);
}

bool hardware::Robot::movePtp(const rw::trajectory::QPath &path, const std::vector<double> &speed, const std::vector<double> &blend) {
    if(!_connected){
        rw::common::Log::log().info() << "ERROR: Not connected!" << std::endl;
        return false;
    } 
    return doFollowPath(path, speed, blend);
}

bool hardware::Robot::moveLin(const rw::math::Transform3D<double> &to, const double &speed, const double &blend) {
    if(!_connected){
        // use speed to make sure no velocity constraints apply.
        // do this by going through the path and calc velocity / vel. constraint and scale (reduce) velocity
        rw::common::Log::log().info() << "ERROR: Not connected!" << std::endl;
        return false;
    }
    return doMoveLin(to, speed, blend);
}

bool hardware::Robot::moveLin ( const rw::math::Q& pos, const double& speed, const double& blend ) {
    if(!_connected){
        // use speed to make sure no velocity constraints apply.
        // do this by going through the path and calc velocity / vel. constraint and scale (reduce) velocity
        rw::common::Log::log().info() << "ERROR: Not connected!" << std::endl;
        return false;
    }
    caros_universalrobot::UrServiceMoveLinQGoal srv;
    srv.request.speed = speed;
    srv.request.blend = blend;
    srv.request.target = pos.toStdVector();

    if (ros::service::call("/caros_universalrobot/move_lin", srv)){
        if(srv.response.success){
            return true;
        }
    }
    rw::common::Log::log().info() << "Was not able to call move_lin" << std::endl;
    return false;
}

rw::math::Q hardware::Robot::getConfiguration(){
    _configuration = getCurrentJointConfiguration();
    return _configuration;
}

rw::math::Q hardware::Robot::getCurrentJointConfiguration() {
    /* Make sure to get and operate on fresh data from the serial device
     * It's assumed that the serial device is not moving
     * ^- That could be asserted / verified using sdsip.isMoving()
     * However other sources could invoke services on the UR that causes it to move...
     */
    //rw::common::Log::log().info() << "Getting joint constraints..\n";
    ros::Time current_timestamp = ros::Time::now();
    ros::Time obtained_timestamp = _sdsip.getTimeStamp();
    ros::Time timer = current_timestamp + ros::Duration(5); // wait up to 5 seconds to see if connecting
    bool timeExpired = false;

    while (current_timestamp > obtained_timestamp && !timeExpired)
    {
        ros::Duration(0.05).sleep();  // In seconds // original 0.1s
        ros::spinOnce();
        obtained_timestamp = _sdsip.getTimeStamp();
        timeExpired = (timer < ros::Time::now());
    }

    if(timeExpired){
       _connected = false;
       return _configuration;
    } else {
        _connected = true;
    }

    return _sdsip.getQ();
}


bool hardware::Robot::isMoving(){
    rw::math::Q first = getCurrentJointConfiguration();
    ros::Duration(0.05).sleep();  // In seconds // original 0.1s
    rw::math::Q second = getCurrentJointConfiguration();
    if (first == second || !_connected){
        return false;
    }
    return true;
}


bool hardware::Robot::doFollowPath(const rw::trajectory::QPath &path){
    for (const rw::math::Q& p : path) {
        if (!_sdsip.movePtp(p,100.0,0)) {
            rw::common::Log::log().error() << "The serial device didn't acknowledge the movePtp command." << std::endl;
            return false;
        }
    }
    return true;
}

bool hardware::Robot::doFollowPath ( const rw::trajectory::QPath& path, const std::vector< double >& speed, const std::vector< double >& blend ) {
    if(path.size() != speed.size() || path.size() != blend.size()){
        return false;
    }
    for (size_t i = 0; i < path.size(); i++) {
        if(speed.at(i) <= 0){
            rw::common::Log::log().error() << "Speed lower than 0 not allowed." << std::endl;
	    return false;
        }
        if (!_sdsip.movePtp(path.at(i), speed.at(i), blend.at(i))) {
            rw::common::Log::log().error() << "The serial device didn't acknowledge the movePtp command." << std::endl;
            return false;
        }
    }
    return true;
}

bool hardware::Robot::doMoveLin(const rw::math::Transform3D<double> &to, const double &speed, const double &blend) {
    if(speed <= 0){
        rw::common::Log::log().error() << "Speed lower than 0 not allowed." << std::endl;
	return false;
    }
    if (!_sdsip.moveLin(to, speed, blend)) {
        rw::common::Log::log().error() << "The serial device didn't acknowledge the movePtp command." << std::endl;
        return false;
    }
    return true;
}


void hardware::Robot::stop(){
    _sdsip.stop();
}


void hardware::Robot::activateForceMode(const rw::math::Wrench6D<double> &wrench, const rw::math::Transform3D<double> &offset, const rw::math::Q &selection, const rw::math::Q &limits){
    // create request
    caros_universalrobot::UrServiceForceModeStart srv;
    for(int i = 0; i < 6; i++){
        srv.request.limits[i] = limits[i];
        srv.request.selection[i] = selection[i];
    }
    srv.request.base2forceFrame = caros::toRos(offset);
    srv.request.wrench = caros::toRos(wrench);

    if (ros::service::call("/caros_universalrobot/force_mode_start", srv)){
        if(srv.response.success){
            rw::common::Log::log().info() << "Force mode was activated!" << std::endl;
        }
    }

}


void hardware::Robot::stopForceMode(){
    // create request
    caros_universalrobot::UrServiceForceModeStop srv;

    if (ros::service::call("/caros_universalrobot/force_mode_stop", srv)){
        if(srv.response.success){
            rw::common::Log::log().info() << "Force mode was deactivated!" << std::endl;
        }
    }
}


void hardware::Robot::setPayload(const double &mass, const rw::math::Vector3D<> &com){
    // create request
    caros_universalrobot::UrServicePayload srv;
    srv.request.mass = mass;
    srv.request.com = {com[0], com[1], com[2]};

    if (ros::service::call("/caros_universalrobot/set_payload", srv)){
        if(srv.response.success){
            rw::common::Log::log().info() << "Payload was set!" << std::endl;
        }
    }
}

