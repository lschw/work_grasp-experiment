#pragma once

#include <ros/ros.h>
#include <ros/io.h>

#include <rw/common/Ptr.hpp>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <cv_bridge/cv_bridge.h>

#include <image_transport/image_transport.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <QThread>

#include <string>
#include <atomic>
#include <condition_variable>
#include <chrono>

#include <eigen3/Eigen/Core>

namespace hardware {

/**
 * @brief The stereocamera class Implements an image getter for a stereocamera.
 */
class Monocamera : public QThread
{
public:
    typedef rw::common::Ptr< Monocamera > Ptr;
    
    struct CameraInfo {
        Eigen::Matrix3d _K = Eigen::MatrixBase< Eigen::Matrix3d >::Identity(); // Camera intrinsics
    };

    ///Note: The constructor will block until connected with roscore
    ///Instead of ros::spin(), start this thread with the start() method
    ///to run the event loop of ros
    Monocamera(const std::string &camera, const std::string encoding = sensor_msgs::image_encodings::MONO8);

    ~Monocamera(){}

    /**
     * @brief run Starter of the thread, called with this.start().
     */
    void run();

    /**
     * @brief getStereoImage Gets the image if it has been updated, if not the image is not given.
     * @param image
     * @return true if the image has been updated, false otherwise.
     */
    bool getImage(cv::Mat &image);

    /**
     * @brief getIdxImage Gets the current image.
     * @param image
     * @return Returns the img index.
     */
    size_t getIdxImage(cv::Mat &image);

    /**
     * @brief quitNow Stops the thread.
     */
    void quitNow();

    void setImageEncoding(const std::string encoding);
    
    bool isOK() const;
    
    void connectToCameraInfo(const std::string &topic);
    
    CameraInfo getCameraInfo() const {
        return _cameraInformation;
    }
    
    const CameraInfo& getCameraInfo() {
        return _cameraInformation;
    }

protected:
  
    /**
     * @brief connectToCameras Connects to the two cameras.
     */
    void connectToCamera();

    /**
     * @brief imageCallbackLeft Algorithm run when new image arives from publisher.
     * @param msg
     */
    void imageCallback(const sensor_msgs::ImageConstPtr& msg);
    
    void cameraInfoCallback(const sensor_msgs::CameraInfoConstPtr &msg);



    // node handles and stuff..
    ros::NodeHandle _nh;
    image_transport::ImageTransport _it;
    image_transport::Subscriber _image_sub;

    // to see if the image has been updated since last pull
    std::atomic_bool _img_updated;

    // disables the ros::spin
    std::atomic_bool _quitProgram;

    const std::string _camera;

    //sync stuff
    cv::Mat _image;
    std::mutex _mutex_image;
    size_t _img_count;
    std::chrono::time_point<std::chrono::high_resolution_clock> _imgTimeStamp;
    
    /**
     * @brief delay between images in seconds (Used to decide if the camera is OK or not).
     * 
     */
    double _interImageDelay; 
    
    std::string _image_encoding;
    
    // camerainfo 
    std::mutex _mutex_camInfo;
    bool _connectedToCameraInfo;
    ros::Subscriber _cameraInfo_sub;
    CameraInfo _cameraInformation;

};

}

