#include "HardwareControl.hpp"

#include <QMessageBox>

#include <rw/common/Log.hpp>



hardware::HardwareControl::HardwareControl():
_shutdownIndicator ( false ), _emgStopSignaled ( false ),
_robot ( NULL ), _gripper ( NULL ),
_robot_default_speed (100.0),
_robot_task_acceptance(FREE_TO_MOVE),
_callback_sampleQ(NULL), _callback_showQpath(NULL),
_hardwareControlThread(std::thread ( &hardware::HardwareControl::hardwareControlThread, this ))
{
}

hardware::HardwareControl::~HardwareControl(){
    _shutdownIndicator = true;
    
    if(_gripper != NULL) {
        delete _gripper;
    }
    if(_robot != NULL) {
        delete _robot;
    }
    
    _hardwareControlThread.join();
}

void hardware::HardwareControl::hardwareControlThread() {
    bool processing_task = false; // used to block the tasks
    while ( !_shutdownIndicator ) {
        usleep ( 10000 );
//         if ( !ros::ok() && _checkbox_remote_recvTrans->isChecked() ) {
//             _checkbox_remote_recvTrans->setChecked ( false );
//             remoteRecieveTransform();
//             rw::common::Log::log().info() << "Turned OFF remote content subscription because ROS is not OK." << std::endl;
//             rw::common::Log::log().error() << "Turned OFF remote content subscription because ROS is not OK." << std::endl;
//         }
//         if ( _checkbox_remote_recvTrans->isChecked() ) {
//             ros::spinOnce();
//         }
        // --- Verify that program is still connected to the hardware ---
        checkHardwareConnections();

        // --- Run HW control ---
        if ( _emgStopSignaled ) {
            _control_task_mutex.lock();
            _control_tasks.clear();
            _control_task_mutex.unlock();
            processing_task = false;
            continue;
        }


        _control_task_mutex.lock();
        if ( isRobotOK() ) {
            _robotCurrentQ = _robot->getConfiguration();
        }
        _control_task_mutex.unlock();
        
        if ( _control_tasks.empty() ) {
            processing_task = false;
            continue;
        }

        _control_task_mutex.lock();
        // tasks are kept in the list so that they can be looked at by other path planners
        ControlTask task = _control_tasks.front();
        // modify speed
        for(size_t i = 0; i < task._speed.size(); i++){
            task._speed.at(i) *= (_robot_default_speed / 100.0);
            if(task._speed.at(i) <= 0.0){
                rw::common::Log::log().info() << "Invalid speed found (speed " << task._speed.at(i) << ")" << std::endl;
                rw::common::Log::log().error() << "Invalid speed found (speed " << task._speed.at(i) << ")" << std::endl;
            }
        }
        _control_task_mutex.unlock();
        

        if ( ( !isRobotOK() && ( task._task == ROBOT_LIN_T || task._task == ROBOT_PTP_Q || task._task == FORCEMODE_ACTIVATE || task._task == FORCEMODE_DEACTIVATE ) ) ||
        ( !isGripperOK() && ( task._task == GRIPPER_GRASP || task._task == GRIPPER_PTP_Q ) ) ) {
            _emgStopSignaled = true;
            rw::common::Log::log().info() << "ERROR: Trying to execute command on not connected hardware." << std::endl;
            continue;
        }

        if ( processing_task ) {
            switch ( task._task ) {
            case ROBOT_LIN_Q:
            case ROBOT_PTP_Q:
                if ( !_robot->isMoving() ) {
                    const double dist = rw::math::MetricUtil::distInf<rw::math::Q> ( _robot->getConfiguration(), task._q_goal.back() );
                    processing_task = false;
                    if ( dist > 0.01 ) {
                        rw::common::Log::log().info() << "Robot did not reach the destination. The norm-Inf distance between\n\t" 
                                                      << _robot->getConfiguration() << "\n\t\tand\n\t" << task._q_goal.back() << "\nis: " << dist 
                                                      << "\nspeed used: " << (task._speed.empty()?std::to_string(task._speed.at(0)):"NA")
                                                      << "\nblend used: " << (task._blend.empty()?std::to_string(task._blend.at(0)):"NA") << std::endl;
                    }
                }
                break;
            case ROBOT_LIN_T:
                if ( !_robot->isMoving() ) {
                    processing_task = false;
                }
                break;
            case GRIPPER_PTP_Q:
                if ( !_gripper->isMoving() ) {
                    const double dist = rw::math::MetricUtil::distInf<rw::math::Q> ( _gripper->getConfiguration(), task._q_goal.back() );
                    processing_task = false;
                    if ( dist > 0.001 ) { // this is meters for gripper Q's
                        rw::common::Log::log().info() << "Gripper did not reach the destinationis at " << _gripper->getConfiguration() 
                                                      << " should be at " << task._q_goal.back() << "\nnorm-Inf distance: " << dist << std::endl;
                        _control_task_mutex.lock(); // Send the command again if it did not reach the destination. Stupid PG70!
                        _control_tasks.push_front ( task );
                        _control_task_mutex.unlock();
                    }
                }
                break;
            case GRIPPER_GRASP:
                if ( !_gripper->isMoving() ) { // TODO: look at time to ensure grasp is firm
                    processing_task = false;
                }
                break;
            case FORCEMODE_ACTIVATE:
            case FORCEMODE_DEACTIVATE:
                processing_task = false;
                break;
            case SAMPLE_ROBOT_T_OFFSET:{
                if ( _robot->isMoving() ){
                    break;
                }
                const rw::math::Q currentQ = _robotCurrentQ;
                if(_callback_sampleQ != NULL){
                    _callback_sampleQ(currentQ, task);
                }
                processing_task = false;
            }
                break;
            default:
                rw::common::Log::log().info() << "Invalid task #" << task._task << " requested in process loop." << std::endl;
                processing_task = false;
                break;
            }
            // remove the last entry if change is signaled
            if ( !processing_task ) {
                _control_task_mutex.lock();
                _control_tasks.pop_front();
                _control_task_mutex.unlock();
            }
        } else {
            processing_task = true; // set this to false, if wished to stay in this part of if for another cycle
            rw::trajectory::QPath path;

            switch ( task._task ) {
            case ROBOT_PTP_Q:
                if ( _robot_task_acceptance == NO_TASK ) {
                    path.push_back ( _robotCurrentQ );
                    path.insert ( path.end(), task._q_goal.begin(), task._q_goal.end() );
                    if(_callback_showQpath != NULL){
                        _callback_showQpath(path);
                    }
                    processing_task = false;
                    _robot_task_acceptance = WAIT_FOR_ACCEPT;
                } else if ( _robot_task_acceptance == TASK_ACCEPTED || _robot_task_acceptance == FREE_TO_MOVE ) {
                    if ( task._q_goal.size() != task._speed.size() || task._q_goal.size() != task._blend.size() ) {
                        std::vector< double > speed(task._q_goal.size(), _robot_default_speed), blend(task._q_goal.size(), 0.0);
                        _robot->movePtp ( task._q_goal );
                    } else {
                        _robot->movePtp ( task._q_goal, task._speed, task._blend );
                    }
                    if( _robot_task_acceptance != FREE_TO_MOVE ){
                        _robot_task_acceptance = NO_TASK;
                    }
                } else {
                    processing_task = false;
                }
                break;
            case ROBOT_LIN_T:
                if ( _robot_task_acceptance == NO_TASK ) {
                    path.push_back ( _robotCurrentQ );
                    path.insert ( path.end(), task._q_goal.begin(), task._q_goal.end() );
                    if(_callback_showQpath != NULL){
                        _callback_showQpath(path);
                    }
                    processing_task = false;
                    _robot_task_acceptance = WAIT_FOR_ACCEPT;
                    // rw::common::Log::log().info() << "Going Lin. to T: " << task._t_goal << std::endl;
                } else if ( _robot_task_acceptance == TASK_ACCEPTED || _robot_task_acceptance == FREE_TO_MOVE ) {
                    double speed = _robot_default_speed, blend = 0.0;
                    if ( task._speed.size() == 1 ) {
                        speed = task._speed.at ( 0 );
                    } else if ( task._speed.size() > 1 ) {
                        rw::common::Log::log().info() << "Too many speeds provided, using defaults." << std::endl;
                    }
                    if ( task._blend.size() == 1 ) {
                        blend = task._blend.at ( 0 );
                    } else if ( task._blend.size() > 1 ) {
                        rw::common::Log::log().info() << "Too many blends provided, using defaults." << std::endl;
                    }
                    _robot->moveLin ( task._t_goal, speed, blend );
                    if( _robot_task_acceptance != FREE_TO_MOVE ){
                        _robot_task_acceptance = NO_TASK;
                    }
                } else {
                    processing_task = false;
                }

                break;
            case ROBOT_LIN_Q:
                if ( _robot_task_acceptance == NO_TASK ) {
                    path.push_back ( _robotCurrentQ );
                    path.insert ( path.end(), task._q_goal.begin(), task._q_goal.end() );
                    if(_callback_showQpath != NULL){
                        _callback_showQpath(path);
                    }
                    processing_task = false;
                    _robot_task_acceptance = WAIT_FOR_ACCEPT;
                    // rw::common::Log::log().info() << "Going Lin. to T: " << task._t_goal << std::endl;
                } else if ( _robot_task_acceptance == TASK_ACCEPTED || _robot_task_acceptance == FREE_TO_MOVE ) {
                    double speed = _robot_default_speed, blend = 0.0;
                    if ( task._speed.size() == 1 ) {
                        speed = task._speed.at ( 0 );
                    } else if ( task._speed.size() > 1 ) {
                        rw::common::Log::log().info() << "Too many speeds provided, using defaults." << std::endl;
                    }
                    if ( task._blend.size() == 1 ) {
                        blend = task._blend.at ( 0 );
                    } else if ( task._blend.size() > 1 ) {
                        rw::common::Log::log().info() << "Too many blends provided, using defaults." << std::endl;
                    }
                    
                    _robot->moveLin ( task._q_goal.front(), speed, blend );
                    if( _robot_task_acceptance != FREE_TO_MOVE ){
                        _robot_task_acceptance = NO_TASK;
                    }
                } else {
                    processing_task = false;
                }

                break;
            case GRIPPER_PTP_Q:
                if ( task._q_goal.size() != 1 || task._q_goal.at(0).size() != 1 ) {
                    rw::common::Log::log().info() << "Wrong Q size given." << std::endl;
                } else {
                    _gripper->open ( task._q_goal.at ( 0 ) );
                }
                break;
            case GRIPPER_GRASP:
                if ( task._q_goal.size() != 1 || task._q_goal.at(0).size() != 1 ) {
                    rw::common::Log::log().info() << "Wrong Q size given." << std::endl;
                } else {
                    _gripper->grip ( task._q_goal.at ( 0 ) );
                }
                break;
            case FORCEMODE_ACTIVATE:
//                 activateForceMode();
                rw::common::Log::log().info() << "Not implemented yet..." << std::endl;
                break;
            case FORCEMODE_DEACTIVATE:
                _robot->stopForceMode();
                break;
            case SAMPLE_ROBOT_T_OFFSET:
                break;
            default:
                rw::common::Log::log().info() << "Invalid task #" << task._task << " requested." << std::endl;
                processing_task = false;
                _control_task_mutex.lock();
                _control_tasks.pop_front();
                _control_task_mutex.unlock();
                break;
            }
        }
    }
    rw::common::Log::log().info() << "Control Thread was deactivated, hardware movement can hence not be invoked. To reactivate restart this plugin." << std::endl;
    rw::common::Log::log().error() << "Control Thread was deactivated, hardware movement can hence not be invoked. To reactivate restart this plugin." << std::endl;
}

rw::math::Q hardware::HardwareControl::getRobotConfig() {
    rw::math::Q config = _robotCurrentQ;
    // loop through to see if the robots pose is modified
    for ( std::list< ControlTask >::iterator it = _control_tasks.begin(); it != _control_tasks.end(); it++ ) {
        if ( ( *it )._task == TaskRequest::ROBOT_LIN_T || ( *it )._task == TaskRequest::ROBOT_PTP_Q ) {
            if ( ( *it )._q_goal.size() > 0 ) {
                config = ( *it )._q_goal.back();
            }
        }
    }
    return config;
}

bool hardware::HardwareControl::checkHardwareConnections() {
    bool robot = false, gripper = false;
    if ( _robot != NULL ) {
        _robot->getConfiguration(); // used to update and check if still connected
        robot = _robot->isOK();
    }
    if ( _gripper != NULL ) {
        _gripper->getConfiguration(); // used to update and check if still connected
        gripper = _gripper->isOK();
    }
    return (robot && gripper);
}

bool hardware::HardwareControl::isGripperOK() const {
    return (_gripper != NULL && _gripper->isOK());
}

bool hardware::HardwareControl::isRobotOK() const {
    return (_robot != NULL && _robot->isOK());
}

void hardware::HardwareControl::stopRobot() {
    if ( _robot != NULL ) {
        _robot->stop();
    }
    _emgStopSignaled = true;
    _control_task_mutex.lock();
    _control_tasks.clear();
    _control_task_mutex.unlock();

    if ( _robot == NULL ) {
        _emgStopSignaled = false;
        return;
    }
    _robot->stop(); // ensure stop in case mutex was not available because new path got started.

    // give warning box and first allow to run on accept
    QMessageBox msgBox;
    msgBox.setText ( "Robot STOP was invoked." );
    msgBox.setInformativeText ( "Do you want to enable the robot?" );
    msgBox.setStandardButtons ( QMessageBox::Ok );
    msgBox.setIcon ( QMessageBox::Critical );
    int ret = msgBox.exec();
    if ( ret == QMessageBox::Ok ) {
        _emgStopSignaled = false;
    } else {
        throw std::runtime_error ( "Error occured, not a known btn pressed." );
    }

}

bool hardware::HardwareControl::isBusy() const {
    return taskCount() > 0;
}

size_t hardware::HardwareControl::taskCount() const {
    return _control_tasks.size();
}

void hardware::HardwareControl::connnectToGripper ( const std::string& name ) {
    if ( _gripper != NULL ) {
        delete _gripper;
    }
    _gripper = new Gripper ( name );
}

void hardware::HardwareControl::connnectToRobot ( const std::string& name ) {
    if ( _robot != NULL ) {
        delete _robot;
    }
    _robot = new Robot ( name );
}

void hardware::HardwareControl::setCallbackSampleQ ( std::function< void(const rw::math::Q, ControlTask) > func ) {
    _callback_sampleQ = func;
}

void hardware::HardwareControl::setCallbackShowQpath ( std::function< void(const rw::trajectory::QPath) > func ) {
    _callback_showQpath = func;
}


void hardware::HardwareControl::setRobotDefaultSpeed ( const double speed ) {
    if(speed > 100.0){
        _robot_default_speed = 100.0;
    } else if(speed < 0.0){
        _robot_default_speed = 0.0;
    } else {
        _robot_default_speed = speed;
    }
}

void hardware::HardwareControl::addTask ( const hardware::HardwareControl::ControlTask& task ) {
    std::lock_guard<std::mutex> lock(_control_task_mutex);
    _control_tasks.push_back(task);
}

void hardware::HardwareControl::acceptPath() {
    if(_robot_task_acceptance == WAIT_FOR_ACCEPT){
        _robot_task_acceptance = TASK_ACCEPTED;
    } else if (_robot_task_acceptance  != FREE_TO_MOVE){
        _robot_task_acceptance = NO_TASK;
    }
}

void hardware::HardwareControl::setFreeToMove ( const bool movement ) {
    _robot_task_acceptance = NO_TASK;
    if(movement){
        _robot_task_acceptance = FREE_TO_MOVE;
    }
}
