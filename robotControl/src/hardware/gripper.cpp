#include "gripper.hpp"

#include <rw/common/Log.hpp>

#include <stdexcept>


hardware::Gripper::Gripper(const std::string actingNamespace) : 
    _nodehandle("~"), _sdsip(_nodehandle, actingNamespace),
    _connected(false), _configuration(rw::math::Q(1, 0.0))
  // sdsip needs the name of the node to connect to, in this case the caros_ur..
{
    // test if conenction in established
    _configuration = getCurrentJointConfiguration();

    if(!_connected){
        throw std::runtime_error("Could not connect to gripper!");
    }
}

hardware::Gripper::~Gripper(){}

bool hardware::Gripper::isOK() const {
    return _connected;
}

rw::math::Q hardware::Gripper::getConfiguration() {
    _configuration = getCurrentJointConfiguration();
    return _configuration;
}

void hardware::Gripper::grip(const rw::math::Q &force){
    _sdsip.setForceQ(force);
    _sdsip.gripQ(force);
}

void hardware::Gripper::open(const rw::math::Q pos){
    const double MAXPOS = 0.034;
    _sdsip.stopMovement(); // releases the force, but is stuck at the same position

    if(pos[0] <= MAXPOS && pos[0] >= 0 && pos.size() == 1){
        _sdsip.moveQ(pos); // doesn't work currently, idk why.
    } else {
        throw std::runtime_error("Invalid gripper Q given.");
    }
}


bool hardware::Gripper::isMoving() {
    const rw::math::Q first = getCurrentJointConfiguration();
    ros::Duration(0.05).sleep();  // In seconds // original 0.1s
    const rw::math::Q second = getCurrentJointConfiguration();
    if (first == second || !_connected){
        return false;
    }
    return true;
}


rw::math::Q hardware::Gripper::getCurrentJointConfiguration() {
    /* Make sure to get and operate on fresh data from the serial device
     * It's assumed that the serial device is not moving
     * However other sources could invoke services on the UR that causes it to move...
     */

    ros::Time current_timestamp = ros::Time::now();
    ros::Time obtained_timestamp = _sdsip.getTimeStamp();
    ros::Time timer = current_timestamp + ros::Duration(5); // wait up to 5 seconds to see if connecting
    bool timeExpired = false;
    while (current_timestamp > obtained_timestamp && !timeExpired)
    {
        ros::Duration(0.05).sleep();  // In seconds // original 0.1s
        ros::spinOnce();
        obtained_timestamp = _sdsip.getTimeStamp();
        timeExpired = (timer < ros::Time::now());
    }
    if(timeExpired){
       _connected = false;
       return _configuration;
    } else {
        _connected = true;
    }
    
    return _sdsip.getQ();
}

