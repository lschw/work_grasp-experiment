#pragma once

#include <caros/serial_device_si_proxy.h>
#include <caros/serial_device_service_interface.h>
#include <caros/common_robwork.h>

#include <ros/ros.h>

#include <string>

#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/trajectory/Path.hpp>

#include <caros/universal_robots.h>
#include <caros/ur_service_interface.h>

#include <caros_universalrobot/UrServiceForceModeStart.h>
#include <caros_universalrobot/UrServiceForceModeStop.h>
#include <caros_universalrobot/UrServicePayload.h>
#include <caros_universalrobot/UrServiceMoveLinQGoal.h>



/* WARNING: USE AT YOUR OWN RISK! */

namespace hardware {

class Robot
{
public:
    Robot(const std::string actingNamespace);
    
    virtual ~Robot();
    
    bool isOK() const;

    bool movePtp(const rw::math::Q &pos, const double &speed = 100.0, const double &blend = 0.0);
    bool movePtp(const rw::trajectory::QPath &path);
    bool movePtp(const rw::trajectory::QPath &path, const std::vector<double> &speed, const std::vector<double> &blend);
    
    bool moveLin(const rw::math::Transform3D<double> &to, const double &speed = 100.0, const double &blend = 0.0);
    bool moveLin(const rw::math::Q &pos, const double &speed = 100.0, const double &blend = 0.0);
    
    rw::math::Q getConfiguration();
        
    /**
     * @brief activateForceMode ctivates the force mode
     * @param wrench The Forces / torque applieable, Comp: Adjusts the pose to account for it, N-Comp: Accounts for force without deviation from path.
     * @param offset
     * @param selection 6D, 1: Compliant, 0: Non-Compliant. This mode changes the meaning of the other settings.
     * @param limits 6D, Comp: Maximum velocity along/about an axis. N-Comp: Max allowed deviation from point.
     */
    // it shortly jumps a bit down when activating the forcemode at the acurate mass and com.
    // This can be 'fixed' the hakish way by setting the weight higer at first to supply more resistance and then lowering the mass to the wanted
    void activateForceMode(const rw::math::Wrench6D<double> &wrench,  const rw::math::Transform3D<double> &offset, const rw::math::Q &selection, const rw::math::Q &limits);

    void stopForceMode();

    void setPayload(const double &mass, const rw::math::Vector3D<> &com);
    
    bool isMoving();

    void stop();

protected:

    rw::math::Q getCurrentJointConfiguration();    
    
    bool doFollowPath(const rw::trajectory::QPath &path);
    bool doFollowPath(const rw::trajectory::QPath &path, const std::vector<double> &speed, const std::vector<double> &blend);

    bool doMoveLin(const rw::math::Transform3D<double> &to, const double &speed = 100.0, const double &blend = 0.0);


protected:
    ros::NodeHandle _nodehandle;
    caros::SerialDeviceSIProxy _sdsip;

    // used to check if it succeded finding the robot in workspace
    bool _connected;

    rw::models::WorkCell::Ptr _workcell;
    rw::models::Device::Ptr _device;
    

    // used to hold the robot configurations
    rw::math::Q _configuration;
    
};
}
