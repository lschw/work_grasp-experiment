#include "monocamera.hpp"

#include <ratio>

hardware::Monocamera::Monocamera(const std::string &camera, const std::string encoding):
                                     _nh ( "~" ), _it ( _nh ),
                                     _img_updated(false), _quitProgram(false),
                                     _camera(camera),
                                     _img_count ( 0 ), 
                                     _interImageDelay(1.0),
                                     _image_encoding ( encoding ) {
    connectToCamera();
}


void hardware::Monocamera::connectToCamera(){
    _image_sub = _it.subscribe(_camera, 1, &hardware::Monocamera::imageCallback, this);
    ROS_INFO_STREAM("Subscribed to camera '" << _camera << "', using encoding " << _image_encoding << ".");
}

void hardware::Monocamera::connectToCameraInfo ( const std::string& topic ) {
    _cameraInfo_sub = _nh.subscribe(topic, 1, &hardware::Monocamera::cameraInfoCallback, this);
    ROS_INFO_STREAM("Subscribed to camera info at '" << topic << "'.");
}

void hardware::Monocamera::quitNow(){
    _quitProgram = true;
}

void hardware::Monocamera::imageCallback(const sensor_msgs::ImageConstPtr& msg) {
    std::unique_lock<std::mutex> lck(_mutex_image);
    try {
        _image = cv_bridge::toCvCopy(msg, _image_encoding)->image.clone();
        _imgTimeStamp = std::chrono::high_resolution_clock::now();
        ++_img_count;
	_img_updated = true;
    } catch(cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        _quitProgram = true;
        return;
    }
}

void hardware::Monocamera::cameraInfoCallback ( const sensor_msgs::CameraInfoConstPtr& msg ) {
    std::unique_lock<std::mutex> lck(_mutex_camInfo);
    
    // copy the intrinsics
    for(int i = 0; i < 9; i++){
        _cameraInformation._K( std::floor(i / 3), i % 3 ) = msg->K[i];
    }
}

bool hardware::Monocamera::getImage(cv::Mat &image){
    std::unique_lock<std::mutex> lck ( _mutex_image );
    if(!_img_updated || !(_image.cols > 0 && _image.rows > 0)){
        return false;
    }
    _img_updated = false;
    image = _image.clone();
    return true;
}

size_t hardware::Monocamera::getIdxImage(cv::Mat &image) {
    std::unique_lock<std::mutex> lck(_mutex_image);
    if(_image.empty() || !(_image.cols > 0 && _image.rows > 0)){
        return 0;
    }
    image = _image.clone();
    return _img_count;
}

void hardware::Monocamera::run(){
    while(ros::ok() && !_quitProgram) {
        ros::spinOnce();

        ros::Duration(0.01).sleep();
    }
    _quitProgram=false;
}

void hardware::Monocamera::setImageEncoding ( const std::string encoding ) {
    _image_encoding = encoding;
}

bool hardware::Monocamera::isOK() const {
    std::chrono::time_point<std::chrono::high_resolution_clock> now = std::chrono::high_resolution_clock::now();
    std::chrono::duration< double > time_span = std::chrono::duration_cast< std::chrono::duration<double> >(now - _imgTimeStamp);
    if(time_span.count() <= _interImageDelay){
        return true;
    }
    return false;
}


//http://answers.ros.org/question/53234/processing-an-image-outside-the-callback-function/
