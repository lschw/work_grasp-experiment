#pragma once

#include <caros/gripper_si_proxy.h>
#include <caros/gripper_service_interface.h>
#include <caros/common_robwork.h>

#include <ros/ros.h>

#include <string>

#include <rw/math/Q.hpp>

/* WARNING: USE AT YOUR OWN RISK! */

namespace hardware {

class Gripper
{
public:
    Gripper(const std::string actingNamespace);
    
    virtual ~Gripper();
    
    bool isOK() const;
        
    rw::math::Q getConfiguration();
    
    void grip(const rw::math::Q &force);
    
    void open(const rw::math::Q pos = rw::math::Q(1, 0.034));

    bool isMoving();

    
protected:

    rw::math::Q getCurrentJointConfiguration();

    ros::NodeHandle _nodehandle;
    caros::GripperSIProxy _sdsip;
    
    bool _connected;

    // used to hold the  configuration
    rw::math::Q _configuration;
};
}
