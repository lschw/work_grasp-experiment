#include "CSVUtil.hpp"

#include <rw/math/RPY.hpp>
#include <rw/math/Vector3D.hpp>


void util::CSVUtil::saveCSV(const std::string &filename, const std::vector< rw::math::Transform3D<> > &transforms){
    std::vector< rw::math::VectorND<6, double> > data(transforms.size());
    
    // load data into vector array
    for(size_t i = 0; i < transforms.size(); i++){
        const rw::math::RPY<> rpy(transforms.at(i).R());
        for(size_t k = 0; k < 3; k++){
            data.at(i)[k] = transforms.at(i).P()[k];
            data.at(i)[k+3] = rpy[k];
        }
    }
    
    saveCSV<6, double>(filename, data);
}


void util::CSVUtil::loadCSV(const std::string &filename, std::vector< rw::math::Transform3D<> > &transforms){
    std::vector< rw::math::VectorND< 6, double > > data;
    loadCSV<6, double>(filename, data);
    transforms.resize(data.size());
    
    for(size_t i = 0; i < data.size(); i++){
        transforms.at(i) = rw::math::Transform3D<>(
            rw::math::Vector3D<>(data.at(i)[0], data.at(i)[1], data.at(i)[2]), 
            rw::math::RPY<>(data.at(i)[3], data.at(i)[4], data.at(i)[5])
        );
    }
}




