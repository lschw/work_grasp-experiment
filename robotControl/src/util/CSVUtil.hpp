#pragma once

// std lib
#include <string>
#include <vector>
#include <fstream>
#include <exception>
#include <sstream>

// rw
#include <rw/math/Transform3D.hpp>
#include <rw/math/VectorND.hpp>



namespace util {
    
    class CSVUtil {
    public:
        
        CSVUtil(){}
        
        virtual ~CSVUtil(){}
        
        
        static void saveCSV(const std::string &filename, const std::vector< rw::math::Transform3D<> > &transforms);
        
        template< size_t S, typename T = double >
        static void saveCSV(const std::string &filename, const std::vector< rw::math::VectorND< S, T > > &data);
        
        static void loadCSV(const std::string &filename, std::vector< rw::math::Transform3D<> > &transforms);
        
        template< size_t S, typename T = double >
        static void loadCSV(const std::string &filename, std::vector< rw::math::VectorND< S, T > > &data);
        
        template< size_t S, typename T = double >
        static void readCSVstring(const std::string &dataline, rw::math::VectorND< S, T > &data);
        
        
    protected:
        
        
    };
}




template< size_t S, typename T > 
void util::CSVUtil::saveCSV(const std::string &filename, const std::vector< rw::math::VectorND< S, T > > &data){
    std::fstream file(filename, std::fstream::out);
    if(!file.is_open()){
        throw std::runtime_error("Could not open file to save CSV.\n");
    }
    
    const int dimi = S - 1;
    for(size_t i = 0; i < data.size(); i++){
        for(int k = 0; k < dimi; k++){
            file << data.at(i)[k] << ", ";
        }
        file << data.at(i)[ dimi ] << "\n";
    }
    file.close();
}


template< size_t S, typename T > 
void util::CSVUtil::loadCSV(const std::string &filename, std::vector< rw::math::VectorND< S, T > > &data){
    data.clear();
    std::fstream file(filename, std::fstream::in);
    if(!file.is_open()){
        throw std::runtime_error("Could not open the CSV file.\n");
    }
    
    rw::math::VectorND< S, T > res;
    while(!file.eof()){
        std::string strdat;
        std::getline(file, strdat);
        if(strdat.empty() && file.eof()){
            break;
        }
        readCSVstring<S,T>(strdat, res);
        data.push_back(res);
    }
    file.close();
}


template< size_t S, typename T > 
void util::CSVUtil::readCSVstring ( const std::string& dataline, rw::math::VectorND< S, T >& data ) {
    std::stringstream ss(dataline);
    
    T dat;
    size_t i = 0;
    while((ss >> dat) && i < S){
        if(ss.peek() == ',' || ss.peek() == ' '){
            ss.ignore();
        }
        data[i] = dat;
        i++;
    }
    if(i != S){
        throw std::runtime_error("Not the right # of elements present in string.\n");
    }
}
