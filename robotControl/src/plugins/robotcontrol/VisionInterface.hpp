#pragma once

#include <list>
#include <string>

#include <QObject>
#include <QWidget>
#include <QTimer>

#include <rw/math/Transform3D.hpp>
#include <rw/math/RPY.hpp>

#include <opencv2/core.hpp>

#include "hardware/monocamera.hpp"
#include "vision/Templatematching.hpp"
#include "widgets/templateMatchingOptions/BasicOption.hpp"

class QString;

namespace vision {
    
    /**
     * @brief Interface to the Vision algorithms used in the interface.
     * 
     * @long The vision algorithm should be a seperate entity that does not known the robot setup, but is supplied with the needed data form outside.
     */
    class VisionInterface : public QObject {
        Q_OBJECT
    public:
        
        struct VisionData {
            cv::Point2d _observed_objCoords, _gripperTCP;
            double _matchScore = -1;
            cv::Rect _templateFoundRegion, _cutOutRegion;
            rw::math::Transform3D<> _observed_camTobj, _estimated_camTobj;
            cv::Mat _rawImg, _processedImg, _templateImg, _drawedUpon;
            bool _objFound = false;
            
            std::string dataSave() const { // put the data in a line as it is wanted in the .csv
                const rw::math::RPY<> rpy(_observed_camTobj.R());
                const std::string data = std::to_string(_objFound) +
                                   + "," + std::to_string(_matchScore)
                                   + "," + std::to_string(_observed_objCoords.x)
                                   + "," + std::to_string(_observed_objCoords.y)
                                   + "," + std::to_string(_gripperTCP.x)
                                   + "," + std::to_string(_gripperTCP.y)
                                   + "," + std::to_string(rpy[0])
                                   + "," + std::to_string(rpy[1])
                                   + "," + std::to_string(rpy[2])
                                   + "\n";
                return data;
            }
            
        };
        
        VisionInterface(QWidget &parent, QWidget &freespace);
        
        virtual ~VisionInterface();
        
        
        /**
         * @brief (Re)Connects to the mono camera.
         */
        void reconnectToMonoCamera(const std::string &topic);
        
        bool isConnectedToCamera() const;
        
        /**
         * @brief Locates the object as seen in the most recent image.
         * 
         * @param camTobj The transform from the camera to the object (Used to crop the image).
         * @param location The resulting location of the object.
         */
        void locateObject(const rw::math::Transform3D<> &camTobj, VisionData &location);
        
        void connectToCameraInfo(const std::string& topic);

        
    public slots:
        /**
         * @brief Fetches a image from the camera and displays it in a window.
         */
        void updateCamView();
        
        /**
         * @brief Controls the showing of image the camera sees by opening a new thread the visualizes it.
         */
        void toggle_showCameraView(const bool status);
        
        void toggle_showDetectionOutput(const bool status);
        
        /**
         * @brief Sets what the image should be converted to before template is matched (RAW, Canny, ...).
         * 
         * @param type RAW or Canny edges (only two supported at the time of this writing).
         */
        void setDetectionType(const QString &type);
        
        void loadTemplates();
        
        /**
         * @brief Sets the radius of the bounding sphere of the object to detect.
         *        It is used to crop the image according to the estimate object pose.
         * 
         * @param radius The radius in meters.
         */
        void setObjectBoundingRadius(const double radius);
            
        void updateTemplateMatcherOptions();
        
        void setImgStorageLocation();
        
        void setTemplateMatchingMethod(const QString meth);
        
        void recordStaticBackgroundImage();
        
        void activateSmootheningOfTemplateMatches(const int smooth);
        
    protected:
       
                
        cv::Rect getInterestRegion(const rw::math::Transform3D< double >& camTobj, vision::VisionInterface::VisionData& location ) const;
        
        void fitCropBox(cv::Rect &interestregion, const vision::VisionInterface::VisionData& location);
        
        void setChannel(cv::Mat &mat, int channel, unsigned char value);
        
        void removeStaticBackground(cv::Mat &img) const;
        
        // the parent of this class
        QWidget &_parent; 
        // the widget space that can be used for extra userinput
        // currently assumes the widget is empty when gotten and also leaves it again like that.
        QWidget &_widget_options; 
        widget::vision::BasicOption *_templateMatcherUIWidget;
        
        /// the camera itself
        hardware::Monocamera::Ptr _staticMonoCamera;
        // timer to update visual of camera
        QTimer *_timerCamViewUpdate;
        
        /// vision algorithm things
        vision::Templatematching _visionAlgorithm;
        bool _visiualiseOutput;
        double _objectBoundingSphereRadius;
        
        // String with the location to store the imgs recorded
        std::string _imgStorageLocation;
        
        // static background image
        cv::Mat _static_background_image;
        
    };


}


