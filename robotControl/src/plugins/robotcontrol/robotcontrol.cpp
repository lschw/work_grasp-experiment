#include "robotcontrol.hpp"


#include <rwlibs/pathplanners/rrt/RRTPlanner.hpp>
#include <rwlibs/pathplanners/rrt/RRTQToQPlanner.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>
#include <rwlibs/pathoptimization/pathlength/PathLengthOptimizer.hpp>

#include <rw/models/Joint.hpp>
#include <rw/pathplanning/PlannerConstraint.hpp>
#include <rw/pathplanning/QToQPlanner.hpp>
#include <rw/pathplanning/QSampler.hpp>
#include <rw/proximity/CollisionDetector.hpp>
#include <rw/proximity/ProximitySetupRule.hpp>
#include <rw/kinematics/Kinematics.hpp>
#include <rw/trajectory.hpp>
#include <rw/math/Quaternion.hpp>
#include <rw/math/RPY.hpp>
#include <rw/math/MetricUtil.hpp>
#include <rw/math/VectorND.hpp>
#include <rw/trajectory/Path.hpp>

#include <rwlibs/calibration/Calibration.hpp>
#include <rwlibs/calibration/WorkCellCalibration.hpp>
#include <rwlibs/calibration/xml/XmlCalibrationLoader.hpp>

#include <boost/bind.hpp>

#include <limits> // for max of double
#include <ctime>
#include <functional>

#include <QString>
#include <QPushButton>
#include <QLineEdit>
#include <QString>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QProgressBar>
#include <QSpinBox>
#include <QTimer>
#include <QComboBox>
#include <QFileDialog>
#include <QToolTip>

#include "util/CSVUtil.hpp"


#define PG70_GRIPPER_HOME 0.034

#ifdef USE_VISION
#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#endif

void robotControl::updateStatus ( const QString &text ) {
    _lineEdit_status->clear();
    _lineEdit_status->insert ( text );
}


robotControl::robotControl() :
RobWorkStudioPlugin ( "robotControl", QIcon ( ":/rc_icon.png" ) ),
                    _testActive ( false ),
                    _exp_filename(""),
                    _robot_accepted_path ( 0 ),
                    _pickUpTransform(false),
                    _topicName_remoteTransforms ( "" ),
_nh ( NULL ) {
    // first functions that runs when the plugin is loaded
    setupUi ( this );
//    setAttribute(Qt::WA_AlwaysShowToolTips, true);

    /// now connect stuff from the ui component
    // settings
    connect ( _btn_reconnect_robot                , SIGNAL ( pressed() ), this, SLOT ( btnPressed() ) );
    connect ( _btn_reconnect_gripper              , SIGNAL ( pressed() ), this, SLOT ( btnPressed() ) );
    connect ( _settings_defaultconectionselection , SIGNAL ( activated(int) ), this, SLOT ( settings_defaults() ) );
    
    // robot
    connect ( _btn_robotToModel, SIGNAL ( pressed() ), this, SLOT ( btnPressed() ) );
    connect ( _btn_modelToRobot, SIGNAL ( pressed() ), this, SLOT ( btnPressed() ) );
    connect ( _robot_default_speed  , SIGNAL ( valueChanged ( double ) ), this, SLOT ( updateRobotDefaultSpeed(const double) ) );

    // gripepr
    connect ( _btn_home , SIGNAL ( pressed() ), this, SLOT ( btnPressed() ) );
    connect ( _btn_close, SIGNAL ( pressed() ), this, SLOT ( btnPressed() ) );
    connect ( _btn_open , SIGNAL ( pressed() ), this, SLOT ( btnPressed() ) );


    // connect for experiment
    connect ( _btn_exp_loadcsv         , SIGNAL ( pressed() ), this, SLOT ( btnPressed_exp() ) );
    connect ( _btn_exp_setOrigo        , SIGNAL ( pressed() ), this, SLOT ( btnPressed_exp() ) );
    connect ( _btn_exp_start           , SIGNAL ( pressed() ), this, SLOT ( btnPressed_exp() ) );
    connect ( _btn_exp_stop            , SIGNAL ( pressed() ), this, SLOT ( btnPressed_exp() ) );
    connect ( _btn_exp_releaseObj      , SIGNAL ( pressed() ), this, SLOT ( btnPressed_exp() ) );
    connect ( _btn_exp_home            , SIGNAL ( pressed() ), this, SLOT ( btnPressed_exp() ) );
    connect ( _btn_exp_res_fail        , SIGNAL ( pressed() ), this, SLOT ( btnPressed_exp() ) );
    connect ( _btn_exp_res_misalignment, SIGNAL ( pressed() ), this, SLOT ( btnPressed_exp() ) );
    connect ( _btn_exp_res_success     , SIGNAL ( pressed() ), this, SLOT ( btnPressed_exp() ) );
    connect ( _btn_exp_res_maybe       , SIGNAL ( pressed() ), this, SLOT ( btnPressed_exp() ) );
    connect ( _btn_exp_restart         , SIGNAL ( pressed() ), this, SLOT ( btnPressed_exp() ) );
    connect ( _spinBox_exp_testnumber  , SIGNAL ( valueChanged ( int ) ), this, SLOT ( expCount() ) );

    // connections for forcemode
    connect ( _forcemode_toggler, SIGNAL ( clicked() ), this, SLOT ( forcemodetoggle() ) );
    connect ( _forcemode_payload, SIGNAL ( valueChanged ( double ) ), this, SLOT ( setPayload() ) );
    connect ( _payload_zoffset  , SIGNAL ( valueChanged ( double ) ), this, SLOT ( setPayload() ) );

    // connections for the remote control
    connect ( _checkbox_remote_autoMode , SIGNAL ( clicked() ), this, SLOT ( remoteTransformAutoMode() ) );
    connect ( _checkbox_remote_recvTrans, SIGNAL ( clicked() ), this, SLOT ( remoteRecieveTransform() ) );

    connect ( _spinBox_remote_modT_x    , SIGNAL ( valueChanged ( double ) ), this, SLOT ( remoteUpdateObjectPose() ) );
    connect ( _spinBox_remote_modT_y    , SIGNAL ( valueChanged ( double ) ), this, SLOT ( remoteUpdateObjectPose() ) );
    connect ( _spinBox_remote_modT_z    , SIGNAL ( valueChanged ( double ) ), this, SLOT ( remoteUpdateObjectPose() ) );
    connect ( _spinBox_remote_modT_roll , SIGNAL ( valueChanged ( double ) ), this, SLOT ( remoteUpdateObjectPose() ) );
    connect ( _spinBox_remote_modT_pitch, SIGNAL ( valueChanged ( double ) ), this, SLOT ( remoteUpdateObjectPose() ) );
    connect ( _spinBox_remote_modT_yaw  , SIGNAL ( valueChanged ( double ) ), this, SLOT ( remoteUpdateObjectPose() ) );

    connect ( _btn_remote_acceptTransform, SIGNAL ( pressed() ), this, SLOT ( grasp_remoteRecieveTransform() ) );

    // signals for when the robot is "not trusted"
    connect ( _btn_robot_acceptPath, SIGNAL ( pressed() ), this, SLOT ( btn_robot_accept_path() ) );
    connect ( _checkBox_robot_allowRobotToMoveOnItsOwn, SIGNAL ( clicked(bool) ), this, SLOT ( btn_toggle_robot_free_to_move(bool) ) );

    // connection for general
    connect ( _btn_emg_stopRobot, SIGNAL ( pressed() ), this, SLOT ( btnPressed_stopRobot() ) );
    
#ifndef USE_VISION
    // disable camera things if the needed packages are not there
    const QString stab_static_mono_cam = "Camera";
    for(int i = 0; i < _tabWidget->count(); i++){
        if(_tabWidget->tabText(i) == stab_static_mono_cam){
            _tabWidget->removeTab(i);
        }
    }
    _btn_reconnect_staticMonoCam->setEnabled(false);
#else
    // create an instance of the vision interface with reference to this widget
    _visionInterface = new vision::VisionInterface(*this, *_vision_widget);
    connect ( _btn_reconnect_staticMonoCam, SIGNAL ( pressed() ), this, SLOT ( btnPressed() ) );
    connect ( _btn_vision_connectToCameraInfo, SIGNAL ( pressed() ), this, SLOT ( btnPressed() ) );
    
    // camera settings
    connect ( _check_showCameraView, SIGNAL ( clicked(bool) ), _visionInterface,  SLOT(toggle_showCameraView(bool)) );
    connect ( _check_showDetectionOutput, SIGNAL ( clicked(bool) ), _visionInterface,  SLOT(toggle_showDetectionOutput(bool)) );

    // camera algo settings
    connect ( _combo_vision_setImageType, SIGNAL ( currentTextChanged(QString) ), _visionInterface,  SLOT( setDetectionType(QString) ) );
    connect ( _btn_vision_loadTemplates, SIGNAL ( pressed() ), _visionInterface, SLOT ( loadTemplates() ) );
    connect ( _spinBox_vision_objBoundingSphereRadius, SIGNAL ( valueChanged(double) ), _visionInterface, SLOT ( setObjectBoundingRadius(double) ) );
    connect ( _btn_vision_runVisionAlgorithm, SIGNAL ( pressed() ), this, SLOT ( locateObject() ) );
    connect ( _btn_vision_imgStorageFolder, SIGNAL ( pressed() ), _visionInterface, SLOT ( setImgStorageLocation() ) );
    connect ( _combo_vision_matchMethod, SIGNAL ( currentTextChanged(const QString&) ), _visionInterface, SLOT ( setTemplateMatchingMethod(const QString&) ) );
    connect ( _btn_vision_recordStaticBackgroundImage, SIGNAL ( pressed() ), _visionInterface, SLOT ( recordStaticBackgroundImage() ) );
    connect ( _check_vision_smoothTemplateMatches, SIGNAL ( stateChanged(int) ), _visionInterface, SLOT ( activateSmootheningOfTemplateMatches(int) ) );
    
    
    
    _visionInterface->setDetectionType("RAW Image");
#endif


    // -- Intial GUI stuff --
    // don't allow home before it has been set
    _btn_exp_home->setEnabled ( false );
    // only allow start, stop and results before the origo is set
    _btn_exp_start->setEnabled ( false );
    _btn_exp_stop->setEnabled ( false );
    _btn_exp_restart->setEnabled ( false );
    _btn_exp_res_fail->setEnabled ( false );
    _btn_exp_res_maybe->setEnabled ( false );
    _btn_exp_res_misalignment->setEnabled ( false );
    _btn_exp_res_success->setEnabled ( false );

    // don't allow these to be pressed before only remote content s accepted
    _btn_remote_acceptTransform->setEnabled ( false );

    _timer_remote = new QTimer ( this );
    _timer_remote->setInterval ( 500 );
    connect ( _timer_remote, SIGNAL ( timeout() ), this, SLOT ( remoteUpdateObjectPose() ) );

    _timer_gui = new QTimer ( this );
    _timer_gui->setInterval ( 200 );
    connect ( _timer_gui, SIGNAL ( timeout() ), this, SLOT ( updateGUITabs() ) );
    _timer_gui->start();

    // initiate ROS
    std::string name = "rwplugin_robotcontrol";
    int argc = 0;
    ros::init ( argc, NULL, name );
    
    // connect the callback to the hwcontroller
    std::function< void(const rw::math::Q, hardware::HardwareControl::ControlTask) > func_sampleQ = std::bind( &robotControl::callback_saveQ, this, std::placeholders::_1, std::placeholders::_2);
    std::function< void(const rw::trajectory::QPath) > func_showQpath = std::bind( &robotControl::callback_showQpath, this, std::placeholders::_1);
    
    hwcontrol.setCallbackSampleQ(func_sampleQ);
    hwcontrol.setCallbackShowQpath(func_showQpath);
    
}

robotControl::~robotControl() {
    _timer_gui->stop();
}

void robotControl::initialize() {
    // second function that runs when plugin is loaded
    getRobWorkStudio()->stateChangedEvent().add ( boost::bind ( &robotControl::stateChangedListener, this, _1 ), this );
}

void robotControl::open ( rw::models::WorkCell* workcell ) {
    if(workcell->getCalibrationFilename() != ""){
        rw::common::Log::log().info() << "Loaded calibration file from '" << workcell->getFilePath() << workcell->getCalibrationFilename() << "'." << std::endl;
        rwlibs::calibration::WorkCellCalibration::Ptr calibration_loaded = rwlibs::calibration::XmlCalibrationLoader::load ( workcell, workcell->getFilePath() + workcell->getCalibrationFilename() );
        calibration_loaded->apply();
    }
    _wc = workcell;
    _state = getRobWorkStudio()->getState();
    getRobWorkStudio()->updateAndRepaint();
}

void robotControl::close() {
    // run when the wc is changed. Run before the open(wc) is run
}

void robotControl::btnPressed() {
    QObject *obj = sender();
    if ( obj == _btn_reconnect_robot ) {
        // reconnect to ws and find device in such
        QString text = "Reconnecting...";
        updateStatus ( text );

        hwcontrol.connnectToRobot(_lineEdit_robotnamespace->text().toStdString());
        
        _drobot = _wc->findDevice ( _lineEdit_devicename->text().toStdString() );
        if ( _drobot == NULL ) {
            throw std::runtime_error ( "Unable to find device " + _lineEdit_devicename->text().toStdString() + " in the loaded workcell." );
        }
        
        if ( hwcontrol.isRobotOK() ) {
            text = "Connected to " + _lineEdit_devicename->text() + ", NS: " + _lineEdit_robotnamespace->text();

            _metric = rw::math::MetricFactory::makeEuclidean< rw::math::Q >();

            _sdrobot = rw::common::ownedPtr( new rw::models::SerialDevice ( _drobot->getBase(), _drobot->getEnd(), _drobot->getName(), _state ) );
            _invkin = rw::common::ownedPtr( new rw::invkin::ClosedFormIKSolverUR ( _sdrobot, _wc->getDefaultState() ) );
            _invkin->setCheckJointLimits ( true );

            _collDetector = rw::common::ownedPtr( new rw::proximity::CollisionDetector ( _wc, rwlibs::proximitystrategies::ProximityStrategyFactory::makeDefaultCollisionStrategy() ));
	    
	    update_settingsDefault_connections(_lineEdit_devicename->text().toStdString(), _lineEdit_robotnamespace->text().toStdString());

        } else {
            text = "Not Connected to Robot.";
        }
        updateStatus ( text );


    } else if ( obj == _btn_reconnect_gripper ) {
        // reconnect to ws and find device in such
        QString text = "Reconnecting...";
        updateStatus ( text );

        hwcontrol.connnectToGripper( _lineEdit_robotnamespace->text().toStdString() );
        _dgripper = _wc->findDevice ( _lineEdit_devicename->text().toStdString() );
        if ( _dgripper == NULL ) {
            throw std::runtime_error ( "Unable to find device " + _lineEdit_devicename->text().toStdString() + " in the loaded workcell." );
        }

        if ( hwcontrol.isGripperOK() ) {
            text = "Connected to NS: " + _lineEdit_robotnamespace->text();
	    update_settingsDefault_connections(_lineEdit_devicename->text().toStdString(), _lineEdit_robotnamespace->text().toStdString());
        } else {
            text = "Not Connected to Gripper.";
        }
        updateStatus ( text );

        
    } else if ( obj == _btn_reconnect_staticMonoCam ) {
        
#ifdef USE_VISION
        // reconnect to ws and find device in such
        QString text = "Reconnecting...";
        updateStatus ( text );

        _visionInterface->reconnectToMonoCamera(_lineEdit_robotnamespace->text().toStdString());
        
        usleep(500000); // wait half a second to see if imgs arrive
        if(_visionInterface->isConnectedToCamera()){
            text = "Connected to NS: " + _lineEdit_robotnamespace->text();
            updateStatus ( text );
            update_settingsDefault_connections(_lineEdit_devicename->text().toStdString(), _lineEdit_robotnamespace->text().toStdString());
        } else {
            text = "Not Connected to Camera.";
        }
        updateStatus ( text );
        
        _camera = _wc->findFrame ( _lineEdit_devicename->text().toStdString() );
        if(_camera == NULL){
            throw std::runtime_error("Could not find the frame '" + _lineEdit_devicename->text().toStdString() + "' in the wc.");
        }
        
#else
        throw std::runtime_error("ERROR: Somehow made it into camera connection when not supported.");
#endif

    } else if ( obj == _btn_vision_connectToCameraInfo ) {
#ifdef USE_VISION
        // reconnect to ws and find device in such

        if(!_visionInterface->isConnectedToCamera()){
            rw::common::Log::log().info() << "Not connected to the camera." << std::endl;
            return;
        }
        
        const std::string topic = _lineEdit_vision_cameraInfoTopic->text().toStdString();
        _visionInterface->connectToCameraInfo(topic);
        
#else
        throw std::runtime_error("ERROR: Somehow made it into camera connection when not supported.");
#endif
    } else if ( obj == _btn_robotToModel ) {
        if ( !hwcontrol.isRobotOK() ) {
            throw std::runtime_error ( "Is not connected to robot." );
        }
        // send the configuration of the model to the robot (move robot to model)
        const rw::math::Q modelState = _drobot->getQ ( _state );

        goToConfiguration ( hwcontrol.getRobotConfig(), modelState, 100.0, 0.0 );

    } else if ( obj == _btn_modelToRobot ) {
        if ( !hwcontrol.isRobotOK() ) {
            throw std::runtime_error ( "Is not connected to robot." );
        }
        // configure the model into the same position as the robot (move model to robot)
        _drobot->setQ ( hwcontrol.getRobotConfig(), _state );
        getRobWorkStudio()->setState ( _state );

    } else if ( obj == _btn_home ) {
        if ( !hwcontrol.isGripperOK()) {
            throw std::runtime_error ( "Is not connected to gripper." );
        }
        const rw::math::Q qhome( 1, PG70_GRIPPER_HOME );
        hwcontrol.addTask( hardware::HardwareControl::ControlTask(hardware::HardwareControl::TaskRequest::GRIPPER_PTP_Q, qhome) );
        _dgripper->setQ ( qhome, _state );
        getRobWorkStudio()->setState ( _state );

    } else if ( obj == _btn_close ) {
        if ( !hwcontrol.isGripperOK() ) {
            throw std::runtime_error ( "Is not connected to gripper." );
        }
        hwcontrol.addTask( hardware::HardwareControl::ControlTask( hardware::HardwareControl::TaskRequest::GRIPPER_GRASP, rw::math::Q ( 1, _spinBox_close->value() )) );
    } else if ( obj == _btn_open ) {
        if ( !hwcontrol.isGripperOK() ) {
            throw std::runtime_error ( "Is not connected to gripper." );
        }
        const rw::math::Q gripper_app_q ( 1, _spinBox_open->value() );
        hwcontrol.addTask( hardware::HardwareControl::ControlTask( hardware::HardwareControl::TaskRequest::GRIPPER_PTP_Q, gripper_app_q ) );
        _dgripper->setQ ( gripper_app_q, _state );
        getRobWorkStudio()->setState ( _state );
    }


}

void robotControl::updateRobotDefaultSpeed(const double speed) {
    hwcontrol.setRobotDefaultSpeed(speed);
}


void robotControl::loadExperiments () {
    _data_exp.clear();
    int dups = _spinBox_exp_repeats->value();
    if ( dups < 1 ) {
        dups = 1;
    }

    // load the data
    std::vector< rw::math::VectorND<7> > rawdata;
    util::CSVUtil::loadCSV<7, double>(_exp_filename, rawdata);

    // convert the data to Experiment
    _data_exp.resize(rawdata.size() * dups);
    ExperimentalSample indata;

    size_t idx = 0;
    for(size_t i = 0; i < rawdata.size(); i++){
        indata._pos[0] = rawdata[i][0];
        indata._pos[1] = rawdata[i][1];
        indata._pos[2] = rawdata[i][2];
        indata._angle[0] = rawdata[i][3] * rw::math::Deg2Rad;
        indata._angle[1] = rawdata[i][4] * rw::math::Deg2Rad;
        indata._angle[2] = rawdata[i][5] * rw::math::Deg2Rad;
        indata._result_wanted = rawdata[i][6];
        for(size_t d = 0; d < static_cast<size_t>(dups); d++){
            _data_exp.at(idx) = indata;
            ++idx;
        }
    }

    _spinBox_exp_testnumber->setRange ( 0, _data_exp.size() );
}

void robotControl::updateTestCount ( const int count ) {
    testnumber_ = count;

    const int resultwanted = ( _data_exp[count]._result_wanted );
    QString status = "Expected result: ";
    if ( resultwanted == 0 ) {
        status += "Failed";
    } else if ( resultwanted == 1 ) {
        status += "Missalligned";
    } else if ( resultwanted == 2 ) {
        status += "Success";
    } else {
        status += "NA";
    }
    _lineEdit_exp_progress->clear();
    _lineEdit_exp_progress->insert ( status );

    _progressBar_exp->setValue ( count * 100.0 / ( _data_exp.size() - 1.0 ) );

    _spinBox_exp_testnumber->setValue ( count );

}

// returns true if collision free
bool robotControl::checkCollisions ( const rw::math::Q &q, const bool printCollidingFrames ) {
    rw::kinematics::State testState = _state.clone();
    _drobot->setQ ( q,testState );
    rw::proximity::CollisionDetector::QueryResult result;
    if ( _collDetector->inCollision ( testState, &result ) ) {
        if ( printCollidingFrames ) {
            rw::kinematics::FramePairSet &fs = result.collidingFrames;
            rw::common::Log::log().info() << "Got " << fs.size() << " colliding frames.";
            for ( rw::kinematics::FramePairSet::const_iterator it = fs.cbegin(); it != fs.cend(); it++ ) {
                rw::common::Log::log().info() << "\n\t" << ( *it ).first->getName() << " --- " << ( *it ).second->getName();
            }
            rw::common::Log::log().info() << std::endl;
        }
        return false;
    }

    return true;
}


void robotControl::goToConfiguration ( const rw::math::Q &from, const rw::math::Q &to, const double speed, const double blend ) {
    const double epsilon = 0.1;
    const double maxtime_rrt = 10.0; // sec

    rw::pathplanning::PlannerConstraint constraint = rw::pathplanning::PlannerConstraint::make ( _collDetector,_drobot,_state );
    rw::pathplanning::QSampler::Ptr sampler = rw::pathplanning::QSampler::makeConstrained ( rw::pathplanning::QSampler::makeUniform ( _drobot ),constraint.getQConstraintPtr() );
    rw::pathplanning::QToQPlanner::Ptr planner = rwlibs::pathplanners::RRTPlanner::makeQToQPlanner ( constraint, sampler, _metric, epsilon, rwlibs::pathplanners::RRTPlanner::RRTConnect );

    // set the robot to the found configuration if it is a valid location
    if ( !checkCollisions ( to ) || !checkCollisions ( from ) ) {
        throw std::runtime_error ( "Start or/and goal in collision." );
    }

    // generate a path
    rw::trajectory::QPath path;
    if(!planner->query ( from, to, path, maxtime_rrt )){
        throw std::runtime_error("No path was found.");
    }
    rwlibs::pathoptimization::PathLengthOptimizer pathoptimizer(constraint, _metric);
    pathoptimizer.pathPruning ( path );
    pathoptimizer.shortCut(path, 0, 2.0, epsilon);

    std::vector<double> sp, bl;
    sp.assign ( path.size(), speed );
    bl.assign ( path.size(), blend );
    hwcontrol.addTask( hardware::HardwareControl::ControlTask( hardware::HardwareControl::TaskRequest::ROBOT_PTP_Q, path, sp, bl ) );
}

void robotControl::goToTransform ( const rw::math::Q &from, const rw::math::Transform3D<> &T, const double speed, const double blend ) {
    if ( _drobot == NULL ) {
        throw std::runtime_error ( "Robot device not initialized." );
    }
    rw::kinematics::Frame::Ptr urTCP = _drobot->getEnd(); // wc_->findFrame(tcp_name)
    if ( urTCP == NULL ) {
        throw std::runtime_error ( "Invalid end frame found." );
    }
    if ( _tcpFrame == NULL ) {
        throw std::runtime_error ( "TCP frame not specified." );
    }
    const rw::math::Transform3D<> toolTend = rw::kinematics::Kinematics::frameTframe ( &*_tcpFrame, &*urTCP, _state );
    const rw::math::Transform3D<>  actualT = T * toolTend;

    // find the possible configurations to the problem
    std::vector< rw::math::Q > possibleConfigurations, validConfigs;
    // ---- retract ----
    possibleConfigurations = _invkin->solve ( actualT, _state );
    if(possibleConfigurations.empty()){
        throw std::runtime_error("Point out of robots reach.");
    }
    // remove the configs in collision
    for ( size_t i = 0; i < possibleConfigurations.size(); i++ ) {
        if ( checkCollisions ( possibleConfigurations[i] ) ) {
            validConfigs.push_back ( possibleConfigurations[i] );
        }
    }
    if ( validConfigs.empty() ) {
        throw std::runtime_error ( "No configuration was found in goToTransform." );
    }

    // expand the remaining configs
    validConfigs = expandQ ( validConfigs );
    // set the next configuration
    const rw::math::Q nextConfiguration = findNearestQ ( from, validConfigs );

    goToConfiguration ( from, nextConfiguration, speed, blend );
}

rw::math::Q robotControl::findNearestQ ( const rw::math::Q& ref, const std::vector< rw::math::Q >& from ) {
    if ( from.empty() ) {
        throw std::runtime_error ( "No destinations given in findNearestQ." );
    }
    int clos_idx = 0;
    double closest = rw::math::MetricUtil::dist2< rw::math::Q > ( from.at ( clos_idx ), ref );
    // find the closest
    for ( int k = 1; k < static_cast<int> ( from.size() ); k++ ) {
        const double dist = rw::math::MetricUtil::dist2< rw::math::Q > ( from.at ( k ), ref );
        if ( dist < closest ) {
            clos_idx = k;
            closest = dist;
        }
    }
    return from.at ( clos_idx );
}

std::vector< rw::math::Q > robotControl::expandQ ( const std::vector< rw::math::Q > &config ) const {
    if ( _sdrobot == NULL ) {
        throw std::runtime_error ( "Serial device not initialized." );
    }
    std::vector< rw::math::Q > res1 = config;
    std::vector< rw::math::Q > res2;
    const double pi2 = 2*rw::math::Pi;
    rw::math::Q lower, upper;
    std::vector<size_t> indices;
    const std::vector<rw::models::Joint*>& joints = _sdrobot->getJoints();
    size_t idx = 0;
    BOOST_FOREACH ( const rw::models::Joint* joint, joints ) {
        if ( dynamic_cast<rw::models::RevoluteJoint*> ( joints[idx] ) ) {
            for ( int i = 0; i<joint->getDOF(); i++ ) {
                indices.push_back ( idx );
                ++idx;
            }
        } else {
            idx += joint->getDOF();
        }
    }

    lower = _sdrobot->getBounds().first;
    upper = _sdrobot->getBounds().second;

    for ( auto index : indices ) {
        res2.clear();
        for ( auto q : res1 ) {
            double d = q ( index );
            while ( d>lower ( index ) )
                d -= pi2;
            while ( d<lower ( index ) )
                d += pi2;
            while ( d <upper ( index ) ) {
                rw::math::Q tmp ( q );
                tmp ( index ) = d;
                res2.push_back ( tmp );
                d += pi2;
            }
        }
        res1 = res2;
    }
    return res1;
}


void robotControl::goToTransform_lin ( const rw::math::Q &start, const rw::math::Transform3D<> &from, const rw::math::Transform3D<> &to, const double speed, const double blend ) {
    // covnert the transform from UR.base->gripper.tcp to UR.base->UR.tcp
    if ( _drobot == NULL || _drobot->getEnd() == NULL ) {
        throw std::runtime_error ( "Robot device or base frame not initialized." );
    }
    rw::kinematics::Frame::Ptr urTCP = _drobot->getEnd();
    if ( urTCP == NULL ) {
        throw std::runtime_error ( "Invalid end frame found." );
    }
    if ( _tcpFrame == NULL ) {
        throw std::runtime_error ( "TCP frame not specified." );
    }
    
    const rw::math::Transform3D<> toolTend = rw::kinematics::Kinematics::frameTframe ( &*_tcpFrame, &*urTCP, _state );

    // the 10 cm come from settings of the UR being 10 cm of - this can be changed on the UR touch pad
    const rw::math::Transform3D<> newTo = to * toolTend;// * rw::math::Transform3D<>(rw::math::Vector3D<>(0,0,0.1), rw::math::Rotation3D<>::identity());
    const rw::math::Transform3D<> newFrom = from * toolTend;// * rw::math::Transform3D<>(rw::math::Vector3D<>(0,0,0.1), rw::math::Rotation3D<>::identity());

    // generate a more detailed linear path
    const rw::math::Vector3D<> length = newTo.P() - newFrom.P();
    const int steps = length.norm2() / 0.005 + 1; // steps of 0.5 cm
    rw::trajectory::QPath path ( steps );
    path.at ( 0 ) = start;
    for ( int i = 1; i < steps; i++ ) {
        // make the transform to put robot
        const rw::math::Vector3D<> P_intermediate = newFrom.P() + length * static_cast<double> ( i ) / ( static_cast<double> ( steps ) - 1.0 );
        rw::math::Transform3D<> t ( P_intermediate, newFrom.R() );
        // find the possible configurations to the problem
        std::vector< rw::math::Q > possibleConfigurations, validConfigs;
        // ---- retract ----
        possibleConfigurations = _invkin->solve ( t, _state );

        for ( size_t k = 0; k < possibleConfigurations.size(); k++ ) {
            if ( checkCollisions ( possibleConfigurations.at ( k ) ) ) {
                validConfigs.push_back ( possibleConfigurations.at ( k ) );
            }
        }
        if ( validConfigs.empty() ) {
//             rw::common::Log::log().info() << " -- Following collisions where found -- \n";
//             for ( size_t k = 0; k < possibleConfigurations.size(); k++ ) {
//                 rw::common::Log::log().info() << " " << k << ":" << std::endl;
//                 rw::common::Log::log().info() << " Robot Q : " << possibleConfigurations.at(k) << std::endl << " Gripper Q : " << _dgripper->getQ(_state) << std::endl;
//                 checkCollisions ( possibleConfigurations.at ( k ), true );
//             }
//             rw::common::Log::log().info() << " -- Collisions ended --" << std::endl;            
            throw std::runtime_error ( "No valid configuration was found in the linear movement in step " + std::to_string ( i ) + " / " + std::to_string ( steps-1 ) + "." );
        }

        validConfigs = expandQ ( validConfigs );
        // set the next configuration
        path.at ( i ) = findNearestQ ( path.at ( i-1 ), validConfigs );
    }

    // move robot if configuration is valid
    hardware::HardwareControl::ControlTask task;
    if(_exp_moveLin_usingQ->isChecked()){
        task = hardware::HardwareControl::ControlTask(hardware::HardwareControl::TaskRequest::ROBOT_LIN_Q, path.back(), speed, blend);
    } else {
        task = hardware::HardwareControl::ControlTask(hardware::HardwareControl::TaskRequest::ROBOT_LIN_T, newTo, speed, blend);
        task._q_goal = path;
        task._q_goal.erase ( task._q_goal.begin() );
    }
    hwcontrol.addTask(task);
}


void robotControl::executeNextTest() {
    if(_mobject == NULL){
        throw std::runtime_error("Object frame not initialised.");
    }
    if ( _baseFrame == NULL || _tcpFrame == NULL ) {
        throw std::runtime_error ( "Needed frames not initialized." );
    }
    const bool useGripper = (_dgripper != NULL);
    // use the loaded data to find the next configuration transformation before grapping
    const rw::math::Vector3D<> &translation = _data_exp.at ( testnumber_ )._pos;
    const rw::math::Rotation3D<> rotation = _data_exp.at ( testnumber_ )._angle.toRotation3D();

    const rw::math::Transform3D<> gripTransform = _origo * rw::math::Transform3D<> ( translation, rotation );
    const rw::math::Transform3D<> retractTransform = _origo * rw::math::Transform3D<> ( rw::math::Vector3D<> ( 0,0,- _exp_retractDistace->value() ) );
    const rw::math::Transform3D<> approachTransform = gripTransform * rw::math::Transform3D<> ( rw::math::Vector3D<> ( 0,0,-_exp_retractDistace->value() ) );

    bool rule_added = false;
    try {
        // place the object before gripping
        if(useGripper){
            placeObject ( _object );
        }
        
        // retract the gripper relative to base grasp
        if (_exp_onlyMoveLin->isChecked()) { 
            goToTransform_lin ( hwcontrol.getRobotConfig(), _origo, retractTransform, 100.0, 0.0 );
        } else {
            goToTransform ( hwcontrol.getRobotConfig(), retractTransform, 100.0, 0.0 );    
	}
	// go to the pose retracted from the new grasp pose
        goToTransform ( hwcontrol.getRobotConfig(), approachTransform, 100.0, 0.0 );

        // remove gripper from collision setup with the object
        if(useGripper){
            rw::proximity::ProximitySetupRule rule_obj_gripper ( _mobject->getName(), _dgripper->getName() + ".*", rw::proximity::ProximitySetupRule::EXCLUDE_RULE );
            _collDetector->addRule ( rule_obj_gripper );
            rule_added = true;
        }
        
        // moe linearly towards the grasp pose
        goToTransform_lin ( hwcontrol.getRobotConfig(), approachTransform, gripTransform, 70.0, 0.0 );
    } catch (std::runtime_error &err) {
        rw::common::Log::log().info() << "Cought error: " << err.what() << std::endl;
        _data_exp[testnumber_]._pos_grasped = gripTransform.P();
        _data_exp[testnumber_]._angle_grasped = rw::math::RPY<> ( gripTransform.R() );
        if(rule_added){
            rw::proximity::ProximitySetupRule rule_obj_gripper ( _mobject->getName(), _dgripper->getName() + ".*", rw::proximity::ProximitySetupRule::EXCLUDE_RULE );
            _collDetector->removeRule ( rule_obj_gripper );
        }
        throw std::runtime_error("Could not execute the test. " + static_cast<std::string>(err.what()));
    }

    // activate force mode
    if ( _forcemode_enable->isChecked() ) {
        hwcontrol.addTask(hardware::HardwareControl::ControlTask ( hardware::HardwareControl::TaskRequest::FORCEMODE_ACTIVATE ));
    }

    // handle results (measure the actual robot pose)
    hardware::HardwareControl::ControlTask task(hardware::HardwareControl::TaskRequest::SAMPLE_ROBOT_T_OFFSET, _origo);
    task._p_offset = &_data_exp[testnumber_]._pos_grasped;
    task._rpy_offset = &_data_exp[testnumber_]._angle_grasped;
    hwcontrol.addTask(task);
    
    // grip the object
    if(useGripper){
        hwcontrol.addTask( hardware::HardwareControl::ControlTask ( hardware::HardwareControl::TaskRequest::GRIPPER_GRASP, rw::math::Q ( 1, _spinBox_close->value() ) ) );
    }
    
    // deactivate forcemode
    if ( _forcemode_enable->isChecked() ) { 
        hwcontrol.addTask( hardware::HardwareControl::ControlTask ( hardware::HardwareControl::TaskRequest::FORCEMODE_DEACTIVATE ));
    }

    // remove object before moving gripper
    if(useGripper){
        placeObject();
    }
    
    // lift to normal pos
    if(useGripper){
        liftGripper ( hwcontrol.getRobotConfig(), gripTransform, _spin_exp_liftHeight->value(), _exp_liftStraightUp->isChecked() );
    }
    // If the vision is used find the object...
#ifdef USE_VISION
    if(_camera != NULL){
        moveRobotToCamera();
        while(hwcontrol.isBusy()){
            usleep(100000);
        }
        usleep(500000); // make sure it doesn't shake
        locateObject();
    }
#endif
}

void robotControl::locateObject() {
#ifdef USE_VISION
    if(_visionInterface == NULL || !_visionInterface->isConnectedToCamera()){
        rw::common::Log::log().info() << "Vision is not applied because a camera is not connected." << std::endl;
        return;
    }

    const bool giveOutput = (sender() == _btn_vision_runVisionAlgorithm), showImgs = _check_showDetectionOutput->isChecked();
    // To apply the vision, the robot first moves the object up to the camera.
    // The vision system then takes a picture and detects the objects pose.
    
    const rw::math::Transform3D<> camTobj(
        rw::math::Vector3D<>( _spinBox_vision_camTobj_x->value(), 
                              _spinBox_vision_camTobj_y->value(), 
                              _spinBox_vision_camTobj_z->value()), 
        rw::math::RPY<>(_spinBox_vision_camTobj_roll->value() * rw::math::Deg2Rad, 
                        _spinBox_vision_camTobj_pitch->value() * rw::math::Deg2Rad, 
                        _spinBox_vision_camTobj_yaw->value() * rw::math::Deg2Rad)
    );
    
    
    // take image
    vision::VisionInterface::VisionData out;
    _visionInterface->locateObject(camTobj, out);
    if ( !giveOutput ) {
        if ( testnumber_ < static_cast<int>(_data_vision.size()) ) {
            _data_vision.at ( testnumber_ ) = out;
        } else {
            rw::common::Log::log().info() << "Vision output was not stored because no location was given." << std::endl;
        }
    }
    
    // give the output
    if ( giveOutput ) { // output some of the results in the log
        if(out._objFound){
            rw::common::Log::log().info() << " -- The Vision Algorithm Result --" 
                                        << "\n The match found gave a score of: " << out._matchScore
                                        << "\n The observed camTobj is: " << rw::math::RPY<>(out._observed_camTobj.R())
                                        << "\n The observed obj coordinate is: " << out._observed_objCoords
                                        << "\n ----" << std::endl;
        } else {
            rw::common::Log::log().info() << "The object was not found!" << std::endl;
        }
    }
    if(showImgs){ // show the taken and processed imgs
        if(out._objFound){
            const std::string windowName_rawimg = "Raw Camera Image";
            const std::string windowName_processedimg = "Processed Image";
            const std::string windowName_templateimg = "Template Image Matched";
            
            // draw some visual markers on the raw image.
            rw::common::Log::log().info() << " -- Drawing Colour Coding --"
                                        << "\n Blue point : observed obj coordinate."
                                        << "\n Green point: expected gripper tcp location."
                                        << "\n Green rect: Region of the template."
                                        << "\n Red rect  : The croped region."
                                        << "\n ---- " << std::endl;
            
            if(!out._drawedUpon.empty()){
                cv::namedWindow( windowName_rawimg, cv::WINDOW_AUTOSIZE );
                cv::imshow( windowName_rawimg, out._drawedUpon);
            }
            if(!out._processedImg.empty()){
                cv::namedWindow( windowName_processedimg, cv::WINDOW_AUTOSIZE );
                cv::imshow( windowName_processedimg, out._processedImg );
            }
            if(!out._templateImg.empty()){
                cv::namedWindow( windowName_templateimg, cv::WINDOW_AUTOSIZE );
                cv::imshow( windowName_templateimg, out._templateImg );
            }

            cv::waitKey(10);
        } else {
            rw::common::Log::log().info() << "The object was not found!" << std::endl;
        }        
    }
#else
    throw std::runtime_error("This function should not be called when vision system is not enabled.");
#endif
}

void robotControl::moveRobotToCamera() {
#ifdef USE_VISION
    if(!hwcontrol.isRobotOK() || _drobot == NULL){
        throw std::runtime_error("Robot is not enabled.");
    }
    if(_camera == NULL){
        throw std::runtime_error("Camera was not found in the wc.");
    }

    const rw::math::Transform3D<> camTobj(
        rw::math::Vector3D<>( _spinBox_vision_camTobj_x->value(), 
                              _spinBox_vision_camTobj_y->value(), 
                              _spinBox_vision_camTobj_z->value()), 
        rw::math::RPY<>(_spinBox_vision_camTobj_roll->value() * rw::math::Deg2Rad, 
                        _spinBox_vision_camTobj_pitch->value() * rw::math::Deg2Rad, 
                        _spinBox_vision_camTobj_yaw->value() * rw::math::Deg2Rad)
    );
    
    const rw::math::Transform3D<> baseTcam = rw::kinematics::Kinematics::frameTframe(_drobot->getBase(), _camera.get(), _state);
    const rw::math::Transform3D<> baseTobj = baseTcam * camTobj;
    // move robot
    const rw::math::Q config = hwcontrol.getRobotConfig();
    goToTransform(config, baseTobj, 100.0, 0.0);
#else
    throw std::runtime_error("This function should not be called when vision system is not enabled.");
#endif
}

void robotControl::grasp_remoteRecieveTransform() {
    // get the transform from the scene
    if ( !_checkbox_remote_recvTrans->isChecked() ) {
        _btn_remote_acceptTransform->setEnabled ( false );
        throw std::runtime_error ( "Functionality not enabled" );
    }
    if ( _drobot == NULL || _drobot->getBase() == NULL ) {
        throw std::runtime_error ( "Robot device or base frame is NULL." );
    }
    if ( _mobject == NULL ) {
        throw std::runtime_error ( "Object frame is NULL." );
    }
    if ( _dgripper == NULL ) {
        throw std::runtime_error ( "Gripper device not initialized." );
    }
    const rw::math::Transform3D<> rTobj = rw::kinematics::Kinematics::frameTframe ( &*_drobot->getBase(), &*_mobject, _state );
    const rw::math::Transform3D<> objTretact ( rw::math::Vector3D<> ( 0.0, 0.0, -_spinBox_remote_linApprDist->value() ) );
    const rw::math::Transform3D<> rTretract = rTobj * objTretact;

    const rw::math::Q gripper_app_q ( 1, _spinBox_remote_gripperApprQ->value() );
    rw::math::Q currentQ = hwcontrol.getRobotConfig();
    // set the fingers to the wanted "open-ness"
    hwcontrol.addTask(hardware::HardwareControl::ControlTask( hardware::HardwareControl::TaskRequest::GRIPPER_PTP_Q, gripper_app_q ));

    _dgripper->setQ ( gripper_app_q, _state );
    // find the path from robot to retract with the pre-appr obj collidable
    goToTransform ( currentQ, rTretract, 100, 0.0 );

    // ensure that the path from retract to grasp is ok
    if ( _mpreApprObj != NULL ) {
        rw::common::Log::log().error() << "Making " << _mpreApprObj->getName() << " not collidable with " << _drobot->getName() << " and " << _dgripper->getName() << std::endl;
        // ensure it is NOT collidable
        rw::proximity::ProximitySetupRule rule_robot ( _mpreApprObj->getName(), _drobot->getName() + ".*", rw::proximity::ProximitySetupRule::EXCLUDE_RULE );
        rw::proximity::ProximitySetupRule rule_gripper ( _mpreApprObj->getName(), _dgripper->getName() + ".*", rw::proximity::ProximitySetupRule::EXCLUDE_RULE );
        _collDetector->addRule ( rule_robot );
        _collDetector->addRule ( rule_gripper );
    }
    currentQ = hwcontrol.getRobotConfig();

    //goToTransform_lin ( currentQ, rTretract, rTobj, 80.0, 0.0 );
    goToTransform ( currentQ, rTobj, 80.0, 0.0 );

    // grasp the object
    hwcontrol.addTask( hardware::HardwareControl::ControlTask ( hardware::HardwareControl::TaskRequest::GRIPPER_GRASP, rw::math::Q ( 1, _spinBox_close->value() ) ) );
    currentQ = hwcontrol.getRobotConfig();

    // lift it out of the obj
    goToTransform(currentQ, rTretract, 80.0, 0.0 );

    if ( _mpreApprObj != NULL ) {
        rw::common::Log::log().error() << "Removing the collision rules." << std::endl;
        // remove the rule
        rw::proximity::ProximitySetupRule rule_robot ( _mpreApprObj->getName(), _drobot->getName() + ".*", rw::proximity::ProximitySetupRule::EXCLUDE_RULE );
        rw::proximity::ProximitySetupRule rule_gripper ( _mpreApprObj->getName(), _dgripper->getName() + ".*", rw::proximity::ProximitySetupRule::EXCLUDE_RULE );
        _collDetector->removeRule ( rule_robot );
        _collDetector->removeRule ( rule_gripper );
    }

    currentQ = hwcontrol.getRobotConfig();
    rw::math::RPY<> rpy(0.0, 0.0, 160.0 * rw::math::Deg2Rad);
    rw::math::Transform3D<> goToDrop(rw::math::Vector3D<>(0.0, -0.45, 0.34), rpy.toRotation3D());
    goToTransform(currentQ, goToDrop, 100, 0);

    rw::math::Q gripper_open_q ( 1, 0.034 );
    hwcontrol.addTask( hardware::HardwareControl::ControlTask ( hardware::HardwareControl::TaskRequest::GRIPPER_PTP_Q, gripper_open_q ) );

    currentQ = hwcontrol.getRobotConfig();
    rpy = rw::math::RPY<>(75.0 * rw::math::Deg2Rad, -10.0 * rw::math::Deg2Rad, 140.0 * rw::math::Deg2Rad);
    rw::math::Transform3D<> homepose(rw::math::Vector3D<>(0.521, -0.21, 0.54), rpy.toRotation3D());

    goToTransform(currentQ, homepose, 100, 0);

}


void robotControl::liftGripper ( const rw::math::Q &start, const rw::math::Transform3D<> &relativeTo, const double liftheight, const bool relativeToWorld ) {
    // make sure to lift straight up relative to base frame when needed for rotorshaft in fixture
    // lift straight up relative to current pose when lifting free-standing objects
    rw::math::Transform3D<> testTransform;

    const rw::math::Transform3D<> lift = rw::math::Transform3D<> ( rw::math::Vector3D<> ( 0.0, 0.0, liftheight ), rw::math::Rotation3D<>::identity() );
    if ( !relativeToWorld ) {
        testTransform = relativeTo * lift;
    } else {
        testTransform = lift * relativeTo;
    }

    goToTransform_lin ( start, relativeTo, testTransform, 80.0, 0.0 );
}

void robotControl::placeObject ( const rw::math::Transform3D<> &t ) {
    // find object
    if ( _mobject != NULL ) {
        _mobject->setTransform ( t, _state );
        _mobject->moveTo(t, _state);
        getRobWorkStudio()->setState ( _state );
    } else {
        throw std::runtime_error("Object frame was not found.");
    }
}

void robotControl::expCount() {
    testnumber_ = _spinBox_exp_testnumber->value();
}


void robotControl::saveResults() const {
    // output
    if(_exp_filename.empty()){
        throw std::runtime_error("No experiment input file was set.");
    }
    std::string file = _exp_filename;
    file.erase ( file.size() - 4 );
    file += "_result.csv";
    std::ofstream outCSVFile( file, std::ofstream::out );
    if ( ! outCSVFile.is_open() ) {
        throw std::runtime_error("File '" + file + "' for output could not be opened.");
    } 
    for ( int i = 0; i < static_cast<int> ( _data_exp.size() ); i++ ) {
        outCSVFile << _data_exp[i].dataSave();
    }
    outCSVFile.close();

#ifdef USE_VISION
    // store the vision data in a csv
    std::string file_vis = _exp_filename;
    file_vis.erase ( file_vis.size() - 4 );
    file_vis += "_result_vision.csv";
    std::ofstream outCSVFile_vis( file_vis, std::ofstream::out );
    if ( ! outCSVFile_vis.is_open() ) {
        throw std::runtime_error("File '" + file_vis + "' for output could not be opened.");
    } 
    for ( int i = 0; i < static_cast<int> ( _data_vision.size() ); i++ ) {
        outCSVFile_vis << _data_vision[i].dataSave();
    }
    outCSVFile_vis.close();
#endif
}

void robotControl::btnPressed_exp() {
    QObject *obj = sender();
    const bool useGripper = (_dgripper != NULL);
    if ( obj == _btn_exp_start ) {
        if ( _testActive ) {
            throw std::runtime_error ( "The test is already active." );
        }
        rw::common::Log::log().info() << "Starting the tests." << std::endl;
        
        // start the testing
        testnumber_ = 0;

        if ( testnumber_ >= static_cast<int> ( _data_exp.size() ) ) {
            throw std::runtime_error ( "Test Complete!" );
        }
        
        _testActive = true;
	
        _btn_exp_start->setEnabled ( false );
        _btn_exp_stop->setEnabled ( true );
        _btn_exp_restart->setEnabled ( true );
        _btn_exp_res_fail->setEnabled ( true );
        _btn_exp_res_maybe->setEnabled ( true );
        _btn_exp_res_misalignment->setEnabled ( true );
        _btn_exp_res_success->setEnabled ( true );

        hwcontrol.addTask( hardware::HardwareControl::ControlTask( hardware::HardwareControl::TaskRequest::ROBOT_PTP_Q, _robotOrigo ) );        
        if(useGripper){
            hwcontrol.addTask( hardware::HardwareControl::ControlTask( hardware::HardwareControl::TaskRequest::GRIPPER_PTP_Q, rw::math::Q ( 1, _spin_exp_approachQ->value() ) ) );
        }

        executeNextTest();

    } else if ( obj == _btn_exp_stop ) {
        if ( !_testActive ) {
            throw std::runtime_error ( "Test not active." );
        }
        // stop the test and close the csv's
        // output
        saveResults();
        _testActive = false;
        _btn_exp_start->setEnabled ( true );
        _btn_exp_restart->setEnabled ( false );
        _btn_exp_stop->setEnabled ( false );
        _btn_exp_res_fail->setEnabled ( false );
        _btn_exp_res_maybe->setEnabled ( false );
        _btn_exp_res_misalignment->setEnabled ( false );
        _btn_exp_res_success->setEnabled ( false );
        rw::common::Log::log().info() << "Test stopped." << std::endl;

    } else if ( obj == _btn_exp_loadcsv ) {
        // load the csv and prepare the output csv (just append test to filename)
        const QString filename = QFileDialog::getOpenFileName(this, "Open file", "", tr("Task file (*.csv)"));
        _exp_filename = filename.toStdString();
        loadExperiments();
#ifdef USE_VISION
        _data_vision.resize(_data_exp.size());
#endif
        
        rw::common::Log::log().info() << "Loaded " << _data_exp.size() << " test points." << std::endl;

    } else if ( obj == _btn_exp_setOrigo ) {
        if ( !hwcontrol.isRobotOK() || _drobot == NULL) {
            throw std::runtime_error ( "Not connected to the robot." );
        }

        _tcpFrame = _wc->findFrame ( _lineEdit_exp_tcpframe->text().toStdString() ); // find the tool frame
        _baseFrame = _wc->findFrame ( _lineEdit_exp_baseframe->text().toStdString() );
        if ( _tcpFrame == NULL ) {
            throw std::runtime_error ( "Tool NOT found!" );
        }
        _mobject = _wc->findFrame<rw::kinematics::MovableFrame> ( _lineEdit_remote_objName->text().toStdString() );
        if(_mobject == NULL){
            throw std::runtime_error ( "Object frame NOT found!" );
        }

        _robotOrigo = hwcontrol.getRobotConfig();
	_drobot->setQ(_robotOrigo, _state);
        _origo = rw::kinematics::Kinematics::frameTframe ( &*_baseFrame, &*_tcpFrame, _state );
        _object = rw::kinematics::Kinematics::worldTframe ( &*_tcpFrame, _state );
        placeObject ( _object );

        _btn_exp_home->setEnabled ( true );
        _btn_exp_start->setEnabled ( true );

        // adjust by multiplying the point transform onto it when executing tests

        rw::common::Log::log().info() << "Setting point of origon to " << _origo << std::endl;
        rw::common::Log::log().info() << "Setting robot config. origon to " << _robotOrigo << std::endl;

    } else if ( obj == _btn_exp_releaseObj ) {
        if ( !_testActive ) {
            throw std::runtime_error ( "Test not started." );
        }
        if ( !hwcontrol.isGripperOK() ) {
            throw std::runtime_error ( "Not connected to gripper." );
        }
        // release the object in gripper
        rw::math::Q gripper_app_q ( 1, _spin_exp_approachQ->value() );
        hwcontrol.addTask( hardware::HardwareControl::ControlTask( hardware::HardwareControl::TaskRequest::GRIPPER_PTP_Q, gripper_app_q ) );
        _dgripper->setQ ( gripper_app_q, _state );
        getRobWorkStudio()->setState ( _state );

    } else if ( obj == _btn_exp_home ) {
        if ( !hwcontrol.isRobotOK() ) {
            throw std::runtime_error ( "Not connected to robot." );
        }
        // return the robot to the origo
        rw::math::Q gripper_app_q ( 1, _spin_exp_approachQ->value() );
        rw::math::Q from = hwcontrol.getRobotConfig();
        goToConfiguration ( from, _robotOrigo, 100.0, 0.0 );
        
        if(useGripper){
            _dgripper->setQ ( gripper_app_q, _state );
            hwcontrol.addTask( hardware::HardwareControl::ControlTask( hardware::HardwareControl::TaskRequest::GRIPPER_PTP_Q, gripper_app_q ) );
        }
        getRobWorkStudio()->setState ( _state );

    } else if ( obj == _btn_exp_res_fail || obj == _btn_exp_res_maybe || obj == _btn_exp_res_misalignment || obj == _btn_exp_res_success) {
        if ( !_testActive ) {
            throw std::runtime_error ( "Test not started." );
        }
        // set the result
        if(obj == _btn_exp_res_fail){
            _data_exp[testnumber_]._result_gained = 0;
        } else if(obj == _btn_exp_res_maybe) {
            _data_exp[testnumber_]._result_gained = 3;
        } else if(obj == _btn_exp_res_misalignment){
            _data_exp[testnumber_]._result_gained = 1;
        } else if(obj == _btn_exp_res_success){
            _data_exp[testnumber_]._result_gained = 2;
        } else {
            throw std::runtime_error("Result for the btn not defined.");
        }

        rw::common::Log::log().info() << "Resulted wanted: " << std::to_string ( _data_exp[testnumber_]._result_wanted )
				      << ", res gained: " << std::to_string ( _data_exp[testnumber_]._result_gained ) << std::endl;

        updateTestCount ( testnumber_ + 1 );

        // output
        saveResults();

        const rw::math::Q gripper_app_q ( 1, _spin_exp_approachQ->value() );

        const rw::math::Q from = hwcontrol.getRobotConfig();
        if(obj == _btn_exp_res_success){
            goToConfiguration ( from, _robotOrigo, 100.0, 0.0 );
        }

        if(useGripper){
            _dgripper->setQ ( gripper_app_q, _state );
            hwcontrol.addTask( hardware::HardwareControl::ControlTask( hardware::HardwareControl::TaskRequest::GRIPPER_PTP_Q, gripper_app_q ) );
        }
        goToConfiguration ( from, _robotOrigo, 100.0, 0.0 );
        getRobWorkStudio()->setState ( _state );

        if ( testnumber_ >= static_cast<int> ( _data_exp.size() ) ) {
            rw::common::Log::log().info() << "Test Complete!" << std::endl;
        }

    } else if ( obj == _btn_exp_restart ) {
        if ( !_testActive ) {
            throw std::runtime_error ( "Test not started." );
        }
        // return to start
        rw::math::Q gripper_app_q ( 1, _spin_exp_approachQ->value() );

        goToConfiguration ( hwcontrol.getRobotConfig(), _robotOrigo, 100.0, 0.0 );

        if(useGripper){
            _dgripper->setQ ( gripper_app_q, _state );
            hwcontrol.addTask( hardware::HardwareControl::ControlTask( hardware::HardwareControl::TaskRequest::GRIPPER_PTP_Q, gripper_app_q ) );
        }
        getRobWorkStudio()->setState ( _state );

        if ( testnumber_ < static_cast<int> ( _data_exp.size() ) ) {
            executeNextTest();
        } else {
            rw::common::Log::log().info() << "Test Complete!" << std::endl;
        }
    } else {
        throw std::runtime_error("Unhandled btn pressed.");
    }
}

void robotControl::settings_defaults() {
    const std::string identifyer = _settings_defaultconectionselection->currentText().toStdString();
    
    if(identifyer == "Custom"){
        return;
    }
    size_t splitPos = identifyer.find("@");
    if(splitPos == std::string::npos){ // if it does not exist, remove it.
        _settings_defaultconectionselection->removeItem(_settings_defaultconectionselection->currentIndex());
        return;
    }
    const std::string modelName(identifyer, 0, splitPos);
    const std::string rosNamespace(identifyer, splitPos+1, std::string::npos);
    
    _lineEdit_devicename->setText(QString::fromStdString(modelName));
    _lineEdit_robotnamespace->setText(QString::fromStdString(rosNamespace));
}

void robotControl::update_settingsDefault_connections ( const std::string& devName, const std::string& rosNamespace ) {
    if(devName.find("@") != std::string::npos || rosNamespace.find("@") != std::string::npos){
        return;
    }
    const std::string attribute = devName + "@" + rosNamespace;
    if(_settings_defaultconectionselection->findText(QString::fromStdString(attribute)) != -1){ // if the attribute exists, exit
        return;
    }
    // otherwise, add the text
    _settings_defaultconectionselection->addItem(QString::fromStdString(attribute));
}

void robotControl::btn_robot_accept_path() {
    hwcontrol.acceptPath();
}

void robotControl::btn_toggle_robot_free_to_move ( bool isfree ) {
    hwcontrol.setFreeToMove(isfree);
}

void robotControl::callback_saveQ ( const rw::math::Q config, hardware::HardwareControl::ControlTask task ) {
    // get the config 
    if(_baseFrame == NULL || _tcpFrame == NULL){
        return;
    }
    rw::kinematics::State tmpstate = _state;
    _drobot->setQ ( config, tmpstate );
    // get the current transform
    const rw::math::Transform3D<> currentT = rw::kinematics::Kinematics::frameTframe ( &*_baseFrame, &*_tcpFrame, tmpstate );
    const rw::math::Transform3D<> graspT =  rw::math::inverse ( task._t_goal ) * currentT;
    if(task._p_offset != NULL){
        *task._p_offset = graspT.P();
    }
    if(task._rpy_offset != NULL){
        *task._rpy_offset = rw::math::RPY<>(graspT.R());
    }
    saveResults();
}

void robotControl::updateGUITabs() {
    // update task count
    const int count = static_cast<int>(hwcontrol.taskCount());
    std::string countText = "00";
    if(count > 99){
        countText = "++";
    } else {
        countText[0] = (((count / 10) % 10) + '0');
        countText[1] = ((count % 10) + '0');
    }
    _task_queuecount->setText(QString::fromStdString(countText));
    
    // find the tabs first
    const QString stab_settings = "Settings", stab_robot = "Robot", 
                      stab_gripper = "Gripper", stab_camera = "Camera", 
                      stab_exp = "Experiment", stab_forcemode = "Forcemode", 
                      stab_remote = "Remote Control";
    int tab_robot = -1, tab_gripper = -1, tab_experiment = -1, 
        tab_cam = -1, tab_forcemode = -1, tab_remotecontrol = -1, tab_settings = -1;
    for(int i = 0; i < _tabWidget->count(); i++){
        if(_tabWidget->tabText(i) == stab_settings){
            tab_settings = i;
        } else if(_tabWidget->tabText(i) == stab_robot){
            tab_robot = i;
        } else if(_tabWidget->tabText(i) == stab_gripper){
            tab_gripper = i;
        } else if(_tabWidget->tabText(i) == stab_camera){
            tab_cam = i;
        } else if(_tabWidget->tabText(i) == stab_exp){
            tab_experiment = i;
        } else if(_tabWidget->tabText(i) == stab_forcemode){
            tab_forcemode = i;
        } else if(_tabWidget->tabText(i) == stab_remote){
            tab_remotecontrol = i;
        } 
    }
    
    if ( hwcontrol.isRobotOK() ) { // only related to robot
        if ( !_tabWidget->isTabEnabled ( tab_robot ) ) {
            _tabWidget->setTabEnabled ( tab_robot, true );
            _tabWidget->setTabEnabled ( tab_forcemode, true );
        }
    } else {
        if ( _tabWidget->isTabEnabled ( tab_robot ) ) {
            _tabWidget->setTabEnabled ( tab_robot, false );
            _tabWidget->setTabEnabled ( tab_forcemode, false );
        }
    }
    if ( hwcontrol.isGripperOK() ) { // only related to gripper
        if ( !_tabWidget->isTabEnabled ( tab_gripper ) ) {
            _tabWidget->setTabEnabled ( tab_gripper, true );
        }
    } else {
        if ( _tabWidget->isTabEnabled ( tab_gripper ) ) {
            _tabWidget->setTabEnabled ( tab_gripper, false );
        }
    }
    #ifdef USE_VISION
    if(_visionInterface != NULL && _visionInterface->isConnectedToCamera()){
        if ( !_tabWidget->isTabEnabled ( tab_cam ) ) {
            _tabWidget->setTabEnabled ( tab_cam, true );
        }
    } else {
        if ( _tabWidget->isTabEnabled ( tab_cam ) ) {
            _tabWidget->setTabEnabled ( tab_cam, false );
        }
    }
    #endif
    if ( hwcontrol.isGripperOK() || hwcontrol.isRobotOK() ) { // needing robot or gripper
        _btn_emg_stopRobot->setEnabled ( true );
        if ( !_tabWidget->isTabEnabled ( tab_experiment ) ) {
            _tabWidget->setTabEnabled ( tab_experiment, true );
            _tabWidget->setTabEnabled ( tab_remotecontrol, true );
        }
    } else {
        _btn_emg_stopRobot->setEnabled ( false );
        if ( _tabWidget->isTabEnabled ( tab_experiment ) ) {
            _tabWidget->setTabEnabled ( tab_experiment, false );
            _tabWidget->setTabEnabled ( tab_remotecontrol, false );
        }
    }
    if ( hwcontrol.isRobotOK() && hwcontrol.isGripperOK() ) { // needing robot or gripper
        if ( !_tabWidget->isTabEnabled ( tab_remotecontrol ) ) {
            _tabWidget->setTabEnabled ( tab_remotecontrol, true );
        }
    } else {
        if ( _tabWidget->isTabEnabled ( tab_remotecontrol ) ) {
            _tabWidget->setTabEnabled ( tab_remotecontrol, false );
        }
    }
}

void robotControl::btnPressed_stopRobot() {
    hwcontrol.stopRobot();
    
}

void robotControl::remoteRecieveTransform() {
    if ( !_checkbox_remote_recvTrans->isChecked() ) {
        _btn_remote_acceptTransform->setEnabled ( false );
        if ( _timer_remote->isActive() ) {
            rw::common::Log::log().info() << "Stoped the timer to update obj pose." << std::endl;
            _timer_remote->stop();
        }
        if ( _topicName_remoteTransforms != "" && _subscription_remoteTransforms.getTopic() == _topicName_remoteTransforms ) {
            _subscription_remoteTransforms.shutdown();
            _topicName_remoteTransforms = "";
        }
        remoteTransformAutoMode();
        rw::common::Log::log().info() << "Turned OFF the reception of remote transforms." << std::endl;
        return;
    }
    // verify that all data given is valid before turning ON
    _f_remote_ref_frame = _wc->findFrame<rw::kinematics::Frame> ( _lineEdit_remote_refFrame->text().toStdString() );
    if ( _f_remote_ref_frame == NULL ) {
        _checkbox_remote_recvTrans->setChecked ( false );
        throw std::runtime_error ( "Reference frame '" +  _lineEdit_remote_refFrame->text().toStdString() + "' was not found." );
    }
    _mobject = _wc->findFrame<rw::kinematics::MovableFrame> ( _lineEdit_remote_objName->text().toStdString() );
    if ( _mobject == NULL ) {
        _checkbox_remote_recvTrans->setChecked ( false );
        throw std::runtime_error ( "Object frame '" + _lineEdit_remote_objName->text().toStdString() + "' was not found." );
    }
    _mpreApprObj = _wc->findFrame<rw::kinematics::MovableFrame> ( _lineEdit_remote_preApprObj->text().toStdString() );
    if ( _mpreApprObj == NULL ) {
        rw::common::Log::log().info() << "Pre-Approach frame '" << _lineEdit_remote_preApprObj->text().toStdString() << "' was not found and hance not using it for path planning." << std::endl;
    }

    _tcpFrame = _wc->findFrame ( _lineEdit_exp_tcpframe->text().toStdString() ); // find the tool frame
    if ( _tcpFrame == NULL ) {
        throw std::runtime_error ( "Tool '" + _lineEdit_exp_tcpframe->text().toStdString() + "' NOT found!" );
    }

    // connect to topic
    _topicName_remoteTransforms = _lineEdit_remote_topic->text().toStdString();
    if ( _topicName_remoteTransforms == "" ) {
        _checkbox_remote_recvTrans->setChecked ( false );
        throw std::runtime_error ( "Topic name to subscribe to must be supplied." );
    }
    if ( _nh == NULL ) {
        _nh = new ros::NodeHandle();
    }
    _subscription_remoteTransforms = _nh->subscribe ( _topicName_remoteTransforms, 1, &robotControl::callback_remoteRecieveTransform, this );

    // start timer if not already active
    if ( !_timer_remote->isActive() ) {
        _timer_remote->start();
	rw::common::Log::log().info() << "Started the timer to update obj pose." << std::endl;
    }

    // if it reached here, all is good
    _btn_remote_acceptTransform->setEnabled ( true );
    rw::common::Log::log().info() << "Turned ON the reception of remote transforms." << std::endl;
}

void robotControl::callback_remoteRecieveTransform ( const geometry_msgs::TransformConstPtr& transform ) {
    // transform consists of: Vector3 translation, Quaternion rotation
    const rw::math::Quaternion<> quat ( transform->rotation.x, transform->rotation.y, transform->rotation.z, transform->rotation.w );
    const rw::math::Transform3D<> camTobj ( rw::math::Vector3D<> ( transform->translation.x, transform->translation.y, transform->translation.z ), quat.toRotation3D() );
    time_t rawtime;
    struct tm * timeinfo;
    std::time ( &rawtime );
    timeinfo = std::localtime ( &rawtime );
    rw::common::Log::log().info() << "Recived the transform:\n" << camTobj << "\nfrom the topic at " << std::asctime ( timeinfo ) << std::endl;

    // load the transform offset
    _remoteTransform_camTobj = camTobj;
    if ( _checkbox_remote_autoMode->isChecked() && _checkbox_remote_recvTrans->isChecked()) {
        rw::common::Log::log().info() << "Checking if robot is available." << std::endl;
        const bool isRobotBusy = hwcontrol.isBusy();
        if ( isRobotBusy ) {
            rw::common::Log::log().info() << "Does not go for the recieved object transform because the robot is busy." << std::endl;
        } else {
            rw::common::Log::log().info() << "Signalling to pick up the transform." << std::endl;
            _pickUpTransform = true;
	}
    }
}

void robotControl::callback_showQpath ( const rw::trajectory::QPath path ) {
    std::vector<rw::kinematics::State> states;
    rw::trajectory::TimedStatePath timed_path;
    states = rw::models::Models::getStatePath ( *_drobot, path, _state );
    
    timed_path = rw::trajectory::TimedUtil::makeTimedStatePath ( *_wc, states );
    
    getRobWorkStudio()->setTimedStatePath ( timed_path );
}


void robotControl::remoteTransformAutoMode() {
    if ( !_checkbox_remote_recvTrans->isChecked() && _checkbox_remote_autoMode->isChecked()) {
        _checkbox_remote_autoMode->setChecked ( false );
        _btn_remote_acceptTransform->setEnabled ( false );
	_pickUpTransform = false;
        rw::common::Log::log().info() << "Could not turn ON Automatic remote control." << std::endl;
    } else if ( _checkbox_remote_autoMode->isChecked() && _checkbox_remote_recvTrans->isChecked() ) {
        _btn_remote_acceptTransform->setEnabled ( false );
        rw::common::Log::log().info() << "Automatic remote control turned ON." << std::endl;
    } else {
        _btn_remote_acceptTransform->setEnabled ( true );
        rw::common::Log::log().info() << "Automatic remote control turned OFF." << std::endl;
    }
}

void robotControl::remoteUpdateObjectPose() {
    if ( !_checkbox_remote_recvTrans->isChecked() ) {
        return;
    }
    if ( _f_remote_ref_frame == NULL ) {
        throw std::runtime_error ( "Remote reference frame is not set." );
    }
    if ( _mobject == NULL ) {
        throw std::runtime_error ( "Object frame is not set." );
    }
    const rw::math::Transform3D<> camTobj = _remoteTransform_camTobj;
    // apply additional transform to obj
    const rw::math::RPY<> rpy ( _spinBox_remote_modT_roll->value() *rw::math::Deg2Rad, _spinBox_remote_modT_pitch->value() *rw::math::Deg2Rad, _spinBox_remote_modT_yaw->value() *rw::math::Deg2Rad );
    const rw::math::Transform3D<> objTmodel ( rw::math::Vector3D<> ( _spinBox_remote_modT_x->value(), _spinBox_remote_modT_y->value(), _spinBox_remote_modT_z->value() ), rpy.toRotation3D() );
    const rw::math::Transform3D<> wTcam = _f_remote_ref_frame->wTf ( _state );
    rw::math::Transform3D<> wTmodel = wTcam * camTobj * objTmodel;
    _mobject->setTransform ( wTmodel, _state );
    if ( _mpreApprObj != NULL ) {
        _mpreApprObj->setTransform ( wTmodel, _state );
    }
    getRobWorkStudio()->setState ( _state );
    
    if(_pickUpTransform){
        _pickUpTransform = false;
        if ( _checkbox_remote_recvTrans->isChecked() && _checkbox_remote_autoMode->isChecked() ) {
            rw::common::Log::log().info() << "Going for the newly detected object." << std::endl;
            grasp_remoteRecieveTransform();
        } else {
            rw::common::Log::log().info() << "Got signaled to go for object when functionality is not enabled." << std::endl;
        }
    }
}

void robotControl::activateForceMode() {
    if ( !hwcontrol.isRobotOK() ) {
        throw std::runtime_error ( "Is not connected to robot!" );
    }
    // get force / torque values
    //    The forces/torques the robot is to apply
    //    to its environment. These values have
    //    different meanings whether they
    //    correspond to a compliant axis or not.
    //    Compliant axis: The robot will adjust its
    //    position along/about the axis in order to
    //    achieve the specified force/torque.
    //    Non-compliant axis: The robot follows
    //    the trajectory of the program but will
    //    account for an external force/torque of
    //    the specified value.
    const rw::math::Wrench6D<double> wrench (
        _forcemode_forcex->value(),
        _forcemode_forcey->value(),
        _forcemode_forcez->value(),
        _forcemode_torquex->value(),
        _forcemode_torquey->value(),
        _forcemode_torquez->value() );

    // set T offest
    //    A pose vector that defines the force
    //    frame relative to the base frame.
    const rw::math::Transform3D<double> offset = rw::math::Transform3D<double>::identity();

    // set selection
    //    A 6d vector that may only contain 0 or 1.
    //    1 means that the robot will be compliant
    //    in the corresponding axis of the task
    //    frame, 0 means the robot is not
    //    compliant along/about that axis.
    const rw::math::Q selection ( 6, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 );

    // set limits
    //    A 6d vector with float values that are
    //    interpreted differently for
    //    compliant/non-compliant axes:
    //    Compliant axes: The limit values for
    //    compliant axes are the maximum
    //    allowed tcp speed along/about the axis.
    //    Non-compliant axes: The limit values for
    //    non-compliant axes are the maximum
    //    allowed deviation along/about an axis
    //    between the actual tcp position and the
    //    one set by the program.
    rw::math::Q limits ( 6 );
    for ( int i = 0; i < static_cast<int> ( limits.size() ); i++ ) {
        limits[i] = _forcemode_speedlimits->value();
    }

    // activate forcemode
    if(hwcontrol.getRobot() != NULL){
        hwcontrol.getRobot()->activateForceMode ( wrench, offset, selection, limits );
    }
}

void robotControl::forcemodetoggle() {
    // first check if force mode is enabled and active
    // if not, disable force mode
    if ( _forcemode_enable->isChecked() && _forcemode_toggler->isChecked() && hwcontrol.isRobotOK() ) {
        _forcemode_feedback->setText ( QString ( "Force Mode Active." ) );
        activateForceMode();
    } else {
        _forcemode_feedback->setText ( QString ( "Force Mode Disabled." ) );
        _forcemode_toggler->setChecked ( false );
        // deactivate force mode
        if ( hwcontrol.isRobotOK() && hwcontrol.getRobot() != NULL ) {
            hwcontrol.getRobot()->stopForceMode();
        }
    }
}


void robotControl::setPayload() {
    if ( !hwcontrol.isRobotOK() || hwcontrol.getRobot() == NULL) {
        throw std::runtime_error ( "Is not connected to robot!" );
    }
    const double mass = _forcemode_payload->value();
    const rw::math::Vector3D<double> com = {0,0,_payload_zoffset->value() };
    hwcontrol.getRobot()->setPayload ( mass, com );
}

void robotControl::stateChangedListener ( const rw::kinematics::State& state ) {
    _state = state;
}

//#if RWS_USE_QT5
//#include <RobWorkStudioConfig.hpp>
//Q_PLUGIN_METADATA(IID "dk.sdu.mip.Robwork.RobWorkStudioPlugin/0.1")
//#else
//#include <QtCore/qplugin.h> // this is needed since rws revision r6026 (trunk)
//Q_EXPORT_PLUGIN(robotControl);
//#endif

#if !RWS_USE_QT5
#include <QtCore/qplugin.h>
Q_EXPORT_PLUGIN ( robotControl );
#endif


