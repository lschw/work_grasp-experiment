#include "pathoptimizer.hpp"


pathOptimizer::pathOptimizer(CollisionChecker &collChecker){
  _collChecker = &collChecker;
}


void pathOptimizer::prune(rw::trajectory::QPath &path, double epsilon){
    int i = 0;
    while(static_cast<int>(path.size()) - 2 > i){
        // if point after next is reachable from current the remove next
        if(!_collChecker->checkCollision(path[i], path[i+2], epsilon)){
            // if path valid, remove next node
            path.erase(path.begin() + i + 1);
        } else{
            i++;
        }
    }


}
