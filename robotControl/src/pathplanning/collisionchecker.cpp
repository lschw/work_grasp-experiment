#include "collisionchecker.hpp"


CollisionChecker::CollisionChecker(rw::kinematics::State &state, rw::models::Device::Ptr &device, rw::proximity::CollisionDetector::Ptr &detector){
    _state = state;
    _device = device;
    _detector = detector;
}

bool CollisionChecker::checkConfiguration(const rw::math::Q &configuration){
    // set device
    _device->setQ(configuration, _state);
    // detect collision
    rw::proximity::CollisionDetector::QueryResult data;
    return _detector->inCollision(_state,&data);
}


bool SFTChecker::checkCollision(const rw::math::Q &from, const rw::math::Q &to, const double &epsilon){
    // dq
    const rw::math::Q dq = to - from;
    // number of configurations
    const int n = (dq.norm2() / epsilon) - 1;
    // step size
    rw::math::Q step = epsilon * (dq / dq.norm2());
    // add configurations to list
    for(int i = 0; i < n; i++){
        const rw::math::Q nextQ = (i + 1) * step + from;
        if(checkConfiguration(nextQ)){
            return true;
        }
    }
    return false;
}


bool USSChecker::checkCollision(const rw::math::Q &from, const rw::math::Q &to, const double &epsilon){
    // dq
    const rw::math::Q dq = to - from;
    // number of configurations
    const int n = (dq.norm2() / epsilon) - 1;
    // step sizes
    rw::math::Q step = dq / (n + 1);
    // add configurations to list
    for(int i = 0; i < n; i++){
        const rw::math::Q nextQ = (i + 1) * step + from;
        if(checkConfiguration(nextQ)){
            return true;
        }
    }
    return false;
}


bool BinChecker::checkCollision(const rw::math::Q &from, const rw::math::Q &to, const double &epsilon){
    // dq
    const rw::math::Q dq = to - from;
    // number of configurations
    const int n = (dq.norm2() / epsilon);
    // levels
    const int levels = std::ceil(std::log2(n));
    // add configurations to list
    for(int i = 0; i < levels; i++){
        const int steps = std::pow(2, i);
        const rw::math::Q step = dq / steps;
        for(int j = 0; j < steps; j++){
            const rw::math::Q nextQ = from + (j + 0.5) * step;
            if(checkConfiguration(nextQ)){
                return true;
            }
        }
    }
    return false;
}


bool EBinChecker::checkCollision(const rw::math::Q &from, const rw::math::Q &to, const double &epsilon){
    // dq
    const rw::math::Q dq = to - from;
    // number of configurations
    const int n = (dq.norm2() / epsilon);
    // levels
    const int levels = std::ceil(std::log2(n));
    // number of divisions made
    const int divisions = std::pow(2,levels) - 1;
    // expansion of dq
    const double expansion = static_cast<double>(divisions) / static_cast<double>(n);
    // expanded dq
    const rw::math::Q edq = dq * expansion;
    // add configurations to list
    for(int i = 0; i < levels; i++){
        const int steps = std::pow(2, i);
        const rw::math::Q step = edq / steps;
        for(int j = 0; j < steps; j++){
            rw::math::Q nextQ = (j + 0.5) * step;
            // check if step exceeds the wanted point
            if(nextQ.norm2() >= dq.norm2()){
                break;
            }
            // increment counter of number of colision checks
            nextQ += from;
            if(checkConfiguration(nextQ)){
                return true;
            }
        }
    }
    return false;
}


